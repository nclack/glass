cmake_minimum_required(VERSION 3.0)
project(glass)

include(cmake/git-tag.cmake)
set_property(GLOBAL PROPERTY USE_FOLDERS TRUE)

include(CMakeToolsHelpers OPTIONAL) # vscode integration

enable_testing()
add_subdirectory(src)
add_subdirectory(test)

set_directory_properties(PROPERTIES VS_STARTUP_PROJECT main)
