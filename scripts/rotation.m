function rotation

%% setup
k=rand(3,1); % axis of rotation
k=[0;0;1];
%k=[0.9 0.9 0.1];

disp(k);
k=k./norm(k);

% when v is orthogonal to k,
% The elipse of rotation will
% be centered at (0,0)

% how to pick a normal
% solve for a vector normal to k
idim=find(k==max(k),1); % zero out the projection with this dimension.
v=[1;1;1];              % it ends up being a divisor so the dimension
v(idim)=0;              % with the biggest k coefficient is picked.
s=dot(v,k);
v(idim)=-s./k(idim);
y=v./norm(v);           % finally make it a unit vector

% last vector is cross between k and v
x=cross(k,y);

v=0.5.*y;

%plot_animated_circle(subplot(121),x,y,k,v);
%render_circle(subplot(122),x,y,k,16,1,[35 35]);
%render_circle_dist(gca(),x,y,k,15,3,[64 64]);

RAD=30;
WIDTH=RAD;

a=render_circle_area(subplot(131),x,y,k,RAD,WIDTH,[64 64]);
b=render_circle_area_mc(subplot(132),x,y,k,RAD,WIDTH,[64 64]);
hax=subplot(133);
imshow(flipud(a'-b'),[-1 1]);
colormap(hax,jet);
colorbar;
%plot_area();
disp(['RMSE ' num2str(sqrt(mean( (a(:)-b(:)).^2 )) )]);
disp('done')

    function plot_area()
        % area of intersection between a unit circle
        % and a half-space a distance d away from
        % the circle's center.
        ds=linspace(0,1,100);
        area=@(d) acos(d)-d.*d.*tan(acos(d));
        plot(ds,area(ds)/pi);
    end

    function R=rotmat(k,theta)
        % Rodrigues' rotation formula
        % https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
        K=[   0  -k(3)  k(2);
            k(3)     0 -k(1);
            -k(2)  k(1)    0]; %k's cross-product matrix
        K2=K*K;
        R=eye(3)+sin(theta).*K+(1-cos(theta)).*K2;
    end

    function im=render_circle_area(hax,x,y,z,radius,width,sz)
        im=zeros(sz);
        rads=[radius-width,radius];

        [ii,jj]=meshgrid(1:sz(1),1:sz(2));
        x=repmat(x(:),1,length(ii(:)));
        y=repmat(y(:),1,length(ii(:)));
        z=repmat(z(:),1,length(ii(:)));
        tic;

        % translate and scale
        r=[ii(:)'-sz(1)/2; jj(:)'-sz(2)/2; zeros(size(ii(:)'))]; % center in box

        % lift (i,j) to plane orthogonal to z that passes
        % through the origin
        r(3,:)= -dot(z(1:2,:),r(1:2,:))./z(3,:);
        xx=dot(r,x);
        yy=dot(r,y);
        d=sqrt(xx.*xx+yy.*yy); % z's=0

        im=im+reshape(...
            area(d,rads(2)) - area(d,rads(1))...
            ,size(im));

        toc

        axes(hax);
        imshow(flipud(im'),[0 1]);
        colormap(hax,gray);
        %imshow(flipud(im'),[0 min(max(im(:)),1)]);
        drawnow;

        function A=area(d,r)
            % area of intersection between circle of radius r, and
            % a "unit" circle, a distance d away.  Assumes 1 << r.
            % area normalized by 2*pi

            rr=sqrt(2); % seems to minimize rmse wrt mc at high angle
            rr=1; % better for enface
            %rr=sqrt(2)/2;
            rr2=rr.*rr;

            dd=abs(d-r);
            th=acos(dd./rr);
            A=(rr2.*th-dd.*dd.*tan(th))./pi;
            onedge=dd<=rr;
            inside=d<r;
            A(~onedge)=0;
            A(inside)=1-A(inside);
        end
    end

    function im=render_circle_area_mc(hax,x,y,z,radius,width,sz)
        N=4000;
        im=zeros(sz);
        rads=[radius-width,radius];

        [ii,jj]=meshgrid(1:sz(1),1:sz(2));
        x=repmat(x(:),1,length(ii(:)));
        y=repmat(y(:),1,length(ii(:)));
        z=repmat(z(:),1,length(ii(:)));
        tic;
        for n=1:N
            % translate and scale
            r=[ii(:)'-sz(1)/2; jj(:)'-sz(2)/2; zeros(size(ii(:)'))]; % center in box
            r(1:2,:)=r(1:2,:)+(rand(2,size(r,2))-0.5);

            % lift (i,j) to plane orthogonal to z that passes
            % through the origin
            r(3,:)= -dot(z(1:2,:),r(1:2,:))./z(3,:);
            xx=dot(r,x);
            yy=dot(r,y);
            d=sqrt(xx.*xx+yy.*yy); % z's=0

            im=im+reshape(...
                (d<=rads(2)) .* (d>rads(1))...
                ,size(im));

        end
        im=im./N;
        toc

        axes(hax);
        imshow(flipud(im'),[]);
        colormap(hax,gray);
        drawnow;

    end


    function render_circle_dist(hax,x,y,z,radius,width,sz)
        N=1;
        im=zeros(sz);
        M=[x y z];
        rads=[radius-width,radius];
        for i=1:sz(1)
            for j=1:sz(2)
                for n=1:N
                    u=[0;0;0]; % [rand(2,1);0];
                    % translate and scale
                    r=[i-1.5-sz(1)/2;j-1.5-sz(2)/2;0]+u; % center in box

                    % lift (i,j) to plane orthogonal to z that passes
                    % through the origin
                    r(3)= -dot(z(1:2),r(1:2))./z(3);
                    xx=dot(r,x);
                    yy=dot(r,y);
                    d=norm([xx yy 0]);


                    if d<rads(1)
                        D=rads(1)-d;
                    elseif d>=rads(2)
                        D=d-rads(2);
                    else % rads(1) <= d < rads(2)
                        D=-min(d-rads(1),rads(2)-d);
                    end

                    im(i,j)=im(i,j)-D;
                end
                im(i,j)=im(i,j)./N;
            end
        end

        axes(hax);
        imshow(flipud(im'),[0 min(max(im(:)),1)]);
        colorbar;
        drawnow;
    end

    function plot_animated_circle(hax,x,y,z,v)
        axes(hax);
        %% rotate a vector v in a circle about k
        thetas=linspace(0,2*pi,30);
        points=zeros(2,numel(thetas)+1);

        for i=1:numel(thetas)
            R=rotmat(z,thetas(i));
            rv=R*v;
            points(:,i)=rv(1:2);

            cla();
            hold on;
            plot([0 z(1)],[0 z(2)],'linewidth',2);
            plot([0 y(1)],[0 y(2)],'k');
            plot([0 x(1)],[0 x(2)],'k');
            plot([0 rv(1)],[0 rv(2)]);
            plot(points(1,1:i),points(2,1:i),'.-');
            hold off;
            axis([-1 1 -1 1]);
            axis square;
            drawnow;
        end

        points(:,end)=points(:,1);

        hold on;
        plot(points(1,:),points(2,:),'.-');
        hold off;

    end

end