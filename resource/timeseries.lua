--local root=[[e:/data/allan/20150716_r3_ss1189da1_frup65_flyb_003]]
local root=[[e:/data/AllanWong/20150716_r3_ss1189da1_frup65_flyb_003]]
--local root=[[c:/data/allan/20150716_r3_ss1189da1_frup65_flyb_003]]
local name=[[20150716_r3_ss1189da1_frup65_flyb_5.1um_00003_%05d.tif]]
local t0=1
local t1=350

local timeseries={}
for i=t0,t1 do
  local path=root .. '/' .. string.format(name,i)
  timeseries[i]=path
end

return timeseries

--[[  NOTES

local a=5

A pretty common approach toward dealing with file series is to use some
filename convention.  However, everyone uses different conventions.
Things get even more complicated when multiple dimensions are encoded like
time, view, color channel, specimen, camera, etc...

The approach I'd like to use here is to use a configuration file to define
how indexes map to filenames.

This script is responsible for returning a table.  The table maps
dimensional indexes to filenames.  I'll assumed that the files all
reference data of the same shape/dimension.  If the files reference
3D volumes, the dimensions used by the table will effectively be
appended.

I'd also like to assume the table describes a dense multi-dimensional
volume.  I may have to deal with a certain level of sparsity, but I'll
cross that bridge when I get there.

Another thing I like about this approach is that it should be simple
to associate other types of data with each file.  For example, we could
use transforms associated with multiple views to align volumes.

--]]

--[[ WHY LUA

I feel like code written Lua ends up looking clean.

It parses fast.  Or at least I feel like it should.  The syntax is much
simpler than, for example, yaml, but it is a full-fledged programming
language.  The fact that it is a programming language is very powerful.

It's simple to embed.

Usually, there's a way to use it without knowing very much about the language.
For example, one way to make the file-series is write

	return {
		'file1',
		'file2',
		...
		'fileN'
	}

If I do things right, I could probably let folks drop the need for the return
statement.
--]]
