-- --[===[
-- 2048x2048x149 u16
return {
    [[E:\Data\Keller\Mmu\15-08-10\SPM00\TM000000\SPM00_TM000000_CM00_CHN00.klb]]
}
--]===]

--[===[
-- 2048x2048x256 u16
root=[[E:\Data\Keller\Mmu\15-08-10\SPM00\TM000138]]
return {
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter10_lambdaTV000000.klb]], 
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter10_lambdaTV000100.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter10_lambdaTV002000.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter20_lambdaTV000000.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter20_lambdaTV000100.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter20_lambdaTV002000.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter30_lambdaTV000000.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter30_lambdaTV000100.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter30_lambdaTV002000.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter40_lambdaTV000000.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter40_lambdaTV000100.klb]],
    root .. '/' .. [[SPM00_TM000138_CM00_CHN00.klb_dec_LR_multiGPU_FWHMpsfZ_07_iter40_lambdaTV002000.klb]],
}
--]===]