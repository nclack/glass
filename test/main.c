#define _CRT_SECURE_NO_WARNINGS
#include <glass.h>
#include <windows.h>
#include <stdio.h>
#include "gl/glcorearb.h" // required for some types in wglext.h
#include "gl/wglext.h"

static void logger(int is_error,const char *file,int line,const char* function,const char *fmt,...) {
    char buf1[1024]={0},buf2[1024]={0};
    va_list ap;
    va_start(ap,fmt);
    vsprintf(buf1,fmt,ap);
    va_end(ap);
#if 0
    sprintf(buf2,"%s(%d): %s()\n\t - %s\n",file,line,function,buf1);
#else
    sprintf(buf2,"%s\n",buf1);
#endif
    OutputDebugStringA(buf2);
}

/*
* BEHAVIORS
*/

static void create_gl_context(HWND h) {
    HDC hdc=GetDC(h);
    PIXELFORMATDESCRIPTOR desc={
        .nSize=sizeof(desc),
        .nVersion=1,
        .dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER,
    };
    int i=ChoosePixelFormat(hdc,&desc);
    SetPixelFormat(hdc,i,&desc);

    HGLRC tmp = wglCreateContext(hdc);
    wglMakeCurrent(hdc, tmp);

    PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribs = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");
    static int const glattriblist[] = {
        WGL_CONTEXT_MAJOR_VERSION_ARB,  	4,
        WGL_CONTEXT_MINOR_VERSION_ARB,  	2,
        WGL_CONTEXT_FLAGS_ARB,          	WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
        0
    };
    HGLRC rc = wglCreateContextAttribs(hdc, 0, glattriblist);
    wglMakeCurrent(hdc, rc);
    wglDeleteContext(tmp);
    ReleaseDC(h,hdc);


    PFNWGLSWAPINTERVALEXTPROC wglSwapInterval=(PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
    wglSwapInterval(0); // 0: turn vsync off, 1: turn vsync on
}

static int is_mouse_outside=1;

static void handle_mouse_tracking(HWND h,UINT msg,WPARAM wparam,LPARAM lparam) {
    TRACKMOUSEEVENT tme={sizeof(tme)};
    tme.dwFlags=TME_LEAVE;
    tme.hwndTrack=h;
    switch(msg) {
        case WM_CREATE: TrackMouseEvent(&tme); break;
        case WM_MOUSEMOVE: {
            if(is_mouse_outside) {
                is_mouse_outside=0;
                TrackMouseEvent(&tme);
            }
        } break;
        case WM_MOUSELEAVE: {
            is_mouse_outside=1;
        } break;
        default:;
    }
}

static void handle_drag_and_drop(HWND h,UINT msg,WPARAM wparam,LPARAM lparam) {
    if(glass_is_dragging()) {
        switch(msg) {            
            case WM_MOUSEMOVE: 
                glass_drag_update(LOWORD(lparam),HIWORD(lparam)); break;
            case WM_MOUSELEAVE:
            case WM_LBUTTONUP: 
                glass_drag_stop(); break;
            default:;
        }
    } else {
        switch(msg) {
            case WM_LBUTTONDOWN: 
                glass_drag_start(LOWORD(lparam),HIWORD(lparam)); break;
            default:;
        }
    }
}

static int is_running=1;

static LRESULT CALLBACK winproc(HWND h,UINT msg,WPARAM wparam,LPARAM lparam) {

    handle_mouse_tracking(h,msg,wparam,lparam);
    handle_drag_and_drop(h,msg,wparam,lparam);

    switch(msg) {
        case WM_SIZE:   glass_resize(LOWORD(lparam),HIWORD(lparam)); break;
        case WM_CREATE: create_gl_context(h); break;
        case WM_DESTROY:
        case WM_CLOSE:  PostQuitMessage(0); is_running=0; break;
        default: return DefWindowProc(h,msg,wparam,lparam);
    }
    return 0;
}

int WinMain(HINSTANCE hinst, HINSTANCE hprev, LPSTR cmd, int show) {
    OutputDebugStringA(glass_version());
    OutputDebugStringA("\n");

    { 
        char buf[1024]={0};
        GetCurrentDirectory(sizeof(buf),buf);
        OutputDebugStringA(buf);
        OutputDebugStringA("\n");
    }

    {
        WNDCLASSA cls={
            .lpszClassName="GlassWindow",
            .hCursor=LoadCursor(0,IDC_ARROW),
            .hIcon=LoadIcon(0,IDI_APPLICATION),
            .lpfnWndProc=winproc,
            .hInstance=hinst
        };
        RegisterClassA(&cls);
    }

    HWND hwnd=CreateWindowA("GlassWindow",glass_version(),
                             WS_OVERLAPPEDWINDOW,
                             CW_USEDEFAULT,CW_USEDEFAULT,
                             CW_USEDEFAULT,CW_USEDEFAULT,
                             NULL,NULL,hinst,NULL);
    ShowWindowAsync(hwnd,show);

    glass_init(logger);

    HDC hdc=GetDC(hwnd);
    while(is_running) {
        MSG msg;
        while(PeekMessage(&msg,hwnd,0,0,PM_REMOVE)) {
            if(msg.message==WM_QUIT)
                goto Quit;
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        glass_update();
        glass_draw();
        SwapBuffers(hdc);
    }
Quit:
    ReleaseDC(hwnd,hdc);
    return 0;
}
