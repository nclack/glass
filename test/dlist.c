#include "../src/dlist.c"
#include <stdio.h>

static void report(const char* testname,int result) {
    printf("%s - %s\n",result?"pass":"FAIL",testname);
}

int main(int argc,char* argv[]) {
    return run_test(report)==0;
}