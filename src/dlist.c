#include "dlist.h"

void dlist_init(struct dlist* dlist) {
    dlist->sentinal.prev=&dlist->sentinal;
    dlist->sentinal.next=&dlist->sentinal;
    dlist->cur=&dlist->sentinal;
}

int dlist_is_empty(const struct dlist *dlist) {
    return dlist->cur==&dlist->sentinal;
}

void dlist_insert(struct dlist* dlist,struct dlist_item *item) {
    item->next=dlist->cur;
    item->prev=dlist->cur->prev;
    dlist->cur->prev->next=item;
    dlist->cur->prev=item;
    dlist->cur=item;
}

void dlist_remove(struct dlist* dlist,struct dlist_item *e) {
    struct dlist_item *n=e->next;
    if(dlist->cur==e)
        dlist->cur=n;
    e->prev->next=n;
    n->prev=e->prev;
    //e->next=e->prev=0; // it's convenient to leave these as-is for iteration
}

void dlist_beg(struct dlist_iterable *it,struct dlist* list) {
    it->list=list;
    it->cur=it->list->cur;
}

void dlist_next(struct dlist_iterable *it) {
    it->cur=it->cur->next;
}
 
int dlist_is_done(struct dlist_iterable *it) {
    return it->cur==&it->list->sentinal;
}

//
// tests
//

#define countof(e) (sizeof(e)/sizeof(*(e)))

static int iterate_empty_list() {
    struct dlist dlist;
    struct dlist_iterable it;
    int isok=1;
    dlist_init(&dlist);
    for(dlist_beg(&it,&dlist);!dlist_is_done(&it);dlist_next(&it)) {
        if(it.cur==&it.list->sentinal)
            isok=0;
    }
    return isok;
}

struct test_item {
    struct dlist_item item;
    int i;
};

static int iterate_nonempty_list() {
    struct dlist dlist;
    struct dlist_iterable it;
    struct test_item items[10];
    int isok=1,acc=0;
    dlist_init(&dlist);
    for(int i=0;i<countof(items);++i){
        items[i].i=i;
        dlist_insert(&dlist,&items[i].item);
    }
    for(dlist_beg(&it,&dlist);isok && !dlist_is_done(&it);dlist_next(&it)) {
        if(it.cur==&it.list->sentinal){
            isok=0;
        } else {
            struct test_item *e=(struct test_item*)it.cur; // assumes item field is at offset 0
            if(e->i!=(countof(items)-acc-1))
                isok=0;            
        }

        ++acc;
    }
    return isok && (acc==countof(items));
}

static int iterate_with_removal() {
    struct dlist dlist;
    struct dlist_iterable it;
    struct test_item items[10];
    int isok=1,acc=0;
    dlist_init(&dlist);
    for(int i=0;i<countof(items);++i){
        items[i].i=i;
        dlist_insert(&dlist,&items[i].item);
    }
    for(dlist_beg(&it,&dlist);isok && !dlist_is_done(&it);dlist_next(&it)) {
        if(it.cur==&it.list->sentinal){
            isok=0;
        } else {
            dlist_remove(&dlist,it.cur);
            struct test_item *e=(struct test_item*)it.cur; // assumes item field is at offset 0
            if(e->i!=(countof(items)-acc-1))
                isok=0;            
        }
        ++acc;
    }
    return isok && (acc==countof(items));
}

static int fail() {
    return 0;
}

#define TEST(e) {#e,e}

struct test {
    const char* name;
    int(*run)();
} tests[]={
     TEST(iterate_empty_list)
    ,TEST(iterate_nonempty_list)
    ,TEST(iterate_with_removal)
    //,TEST(fail)
};

static int run_test(void (*logger)(const char* test,int result)) {
    int all=1;
    for(int i=0;i<countof(tests);++i){
        int result;
        if(!(result=tests[i].run())) // tests return 0 on failure
            all=0;            
        if(logger) logger(tests[i].name,result);
    }
    return all;
}