#include "subvolshow.h"
#include "../pixelfont/src/mingl.h"
#include <math.h> // fabsf
#include <string.h> // memcpy

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct verts {float x,y,u,v;};

static void swap(struct subvshow_view* self);

static const char FRAG[]=GLSL(
    in  vec2 uv;
    out vec4 color;
    uniform sampler2D im;
    uniform float zero,range;

    void main(){
        if( 1.5<(uv.x+uv.y) && (uv.x+uv.y)<1.58) {
            color=vec4(uv,0,1);
            return;
        }
        float v=texture(im,uv).r;
        v=(v*0.01-zero)/range;
        color=vec4(v,v,v,1);
    }
);

static const char VERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    uniform vec2 size;
    out vec2 uv;

    void main(){
        gl_Position=vec4(2*(vert/size)-1,0,1.0f);
        gl_Position.y=-gl_Position.y;
        uv=tex;
    }
);

static const char MEANFRAG[]=GLSL(
    in vec2 uv;
    out vec4 color;    
    uniform float norm; // 1/counts
    uniform sampler2D src,acc;
    void main() {
        vec4  v=texture(acc,uv);
        vec4 vv=texture(src,uv)*100; // This is prescaled to a float.  For an R16, negative values are clamped to 0 and then values are divided by 32k to give unit floats.
        color=mix(v,vv,norm);
    }
);

static const char MCFRAG[]=GLSL(
    in  vec2 uv;
    out vec4 color;
    uniform sampler2D acc; // bdinging=0 
    uniform sampler3D vol; // binding=1
    uniform mat4 T; // T[c][r]
    uniform float size;
    uniform float z,dz;
    uniform float counts; // used for hash function

    // hash functions derived from
    // "Hash without Sine"
    // https://www.shadertoy.com/view/4djSRW
    // returns something between (-0.5,0.5)

    float hash11(float p) {
        vec3 p3 = fract(vec3(p) * 443.8975);
        p3 += dot(p3, p3.yzx + 19.19);
        return fract((p3.x + p3.y) * p3.z)-0.5f;
    }

    vec2 hash23(vec3 p3) {
        p3 = fract(p3 * vec3(443.897, 441.423, 437.195));
        p3 += dot(p3, p3.yzx+19.19);
        return fract((p3.xx+p3.yz)*p3.zy)-0.5f;
    }

    // NITER: 
    // Max projection over NITER random samples
    // Each frame, max the result
    // Adjusting NITER tunes between mean projection (NITER=1) and max projection (say NITER>100).
    const int NITER=100;
    void main() {
        float v=texture(acc,uv).r;
        float vv=0.0; //gl clamps input negative values to zero anyway
        vec2 dxy=hash23(vec3(uv.xy,counts*0.001))/size; // one pixel pertubation
        for(int i=0;i<NITER;++i) {
            float pz=hash11((counts*10+i)*0.001)+0.5;
            vec4 s=T*vec4(uv+dxy,z+pz*dz,1);
            float c=texture(vol,s.xyz).r;
            vv=max(vv,c.r);
        }
        color=vec4(max(v,vv),0,0,1);
    }
);

static const char MCVERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    out vec2 uv;

    void main(){
        gl_Position=vec4(vert,0,1.0f);
        uv=tex;
    }
);

// helpers

static int is_equal_mat44(const float *a,const float *b, const float eps) {
    if(a==b) 
        return 1;
    for(int i=0;i<16;++i) 
        if(fabsf(a[i]-b[i])>eps) 
            return 0;
    return 1;
}

static void mul44(float* out, const float *a, const float *b) {
    for(int c=0;c<4;++c) {
        for(int r=0;r<4;++r) {
            float acc=0.0f;
            for(int i=0;i<4;++i)
                acc+=a[i+4*r]*b[c+4*i];
            out[c+4*r]=acc;
        }
    }
}

// implementation

static void init_display(struct subvshow_display *self) {
    // Shader 
    self->program=mingl_new_program(FRAG,sizeof(FRAG),VERT,sizeof(VERT),0,0);
    mingl_check();
    self->id=(struct subvshow_view_uniforms) {
        .zero=glGetUniformLocation(self->program,"zero"),
        .range=glGetUniformLocation(self->program,"range"),
        .size =glGetUniformLocation(self->program,"size"),
    };
    glUseProgram(self->program);
    glUniform1i(glGetUniformLocation(self->program,"im"),1); // view.mean.acc ends up in TEXTURE1
    {
        GLint sz[4]={0};
        glGetIntegerv(GL_VIEWPORT,sz);
        glUniform2f(self->id.size,(GLfloat)sz[2],(GLfloat)sz[3]);
    }
    glUseProgram(0);

    // Prepping vertex buffers
    glGenBuffers(1,&self->vbo);
    glGenVertexArrays(1,&self->vao);
    glBindVertexArray(self->vao);
    glBindBuffer(GL_ARRAY_BUFFER,self->vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
    glBindVertexArray(0);

    if(!mingl_check())
        DebugBreak();
}

static void init_max_accumulator(struct subvshow_max_accumulator* ctx, int w, int h) {    
    ctx->counts=0;
    ctx->program=mingl_new_program(MCFRAG,sizeof(MCFRAG),MCVERT,sizeof(MCVERT),0,0);
    ctx->internal=GL_RED;
    ctx->type=GL_FLOAT;
    
    ctx->id=(struct subvshow_max_accumulator_uniforms) {
        .counts=glGetUniformLocation(ctx->program,"counts"),
        .size=glGetUniformLocation(ctx->program,"size"),
        .T=glGetUniformLocation(ctx->program,"T"),
        .z=glGetUniformLocation(ctx->program,"z"),
        .dz=glGetUniformLocation(ctx->program,"dz"),
    };

    glUseProgram(ctx->program);
    glUniform1i(glGetUniformLocation(ctx->program,"acc"),0);
    glUniform1i(glGetUniformLocation(ctx->program,"vol"),1);
    glUniform1f(ctx->id.size,(float)w);
    glUseProgram(0);

    glGenTextures(1,&ctx->acc);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,ctx->acc);
    glTexImage2D(GL_TEXTURE_2D,0,ctx->internal,w,h,0,GL_RED,ctx->type,0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glGenTextures(1,&ctx->vol);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture  (GL_TEXTURE_3D,ctx->vol);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_BORDER);
    glBindTexture  (GL_TEXTURE_3D,0);
    
    mingl_check();
}

static void init_mean_accumulator(struct subvshow_mean_accumulator* ctx, int w, int h) {
    ctx->counts=0;
    
    ctx->program=mingl_new_program(MEANFRAG,sizeof(MEANFRAG),MCVERT,sizeof(MCVERT),0,0);
    mingl_check();
    ctx->id=(struct subvshow_mean_accumulator_uniforms) {
        .norm=glGetUniformLocation(ctx->program,"norm"),
    };
    glUseProgram(ctx->program);
    glUniform1i(glGetUniformLocation(ctx->program,"src"),0);  // this is the max accumulator texture.  It's allocated in init_max_accumulator() and stays on GL_TEXTURE0
    glUniform1i(glGetUniformLocation(ctx->program,"acc"),1);
    glUseProgram(0);

    glGenTextures(1,&ctx->acc);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,ctx->acc);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,w,h,0,GL_RGB,GL_FLOAT,0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glBindTexture(GL_TEXTURE_2D,0);
    
    mingl_check();
}

static void init_accumulator(struct subvshow_view* ctx) {
    ctx->w=ctx->h=512;

    init_mean_accumulator(&ctx->mean,ctx->w,ctx->h);
    init_max_accumulator(&ctx->mx,ctx->w,ctx->h);

    {   // init FBO
        glGenFramebuffers(1,&ctx->fbo);
        glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ctx->mx.acc, 0);
        glBindFramebuffer(GL_FRAMEBUFFER,0);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            OutputDebugStringA("ruh roh!\n"); // FIXME: need better error handling
            return;
        }
    }
    mingl_check();

    {   // prep vertex buffer for rendering to texture 
        glGenBuffers(1,&ctx->vbo);
        glGenVertexArrays(1,&ctx->vao);
        glBindVertexArray(ctx->vao);
        glActiveTexture(GL_TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
        glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
        glBindVertexArray(0);

        const float 
            x0=-1.0f,y0=-1.0f,x1=1.0f,y1=1.0f,
            u0= 0.0f,v0= 0.0f,u1=1.0f,v1=1.0f;
        struct verts verts[4]={
            {x0,y0,u0,v0},
            {x0,y1,u0,v1},
            {x1,y1,u1,v1},
            {x1,y0,u1,v0},
        };
        glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
        glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER,0);
    }
    mingl_check();
}

static void set_viewport(struct viewport_listener *self,float w,float h){
    struct subvshow_view *view=containerof(self,struct subvshow_view,viewport_listener);
    struct subvshow_display *display=&view->display;
    display->w=w;
    display->h=h;
    glUseProgram(display->program);
    glUniform2f(display->id.size,(GLfloat)w,(GLfloat)h);
    glUseProgram(0);
}

static void on_rotatable(struct listener* listener) {
    struct rotatable *source=containerof(listener->source,struct rotatable,observable);
    struct subvshow *sink=containerof(listener,struct subvshow,rotatable_listener);
    subvshow_rotate_about_center(sink,source->mat);
}

void subvshow_init(struct subvshow *self){
    struct subvshow_view *view=&self->view;
    view->viewport_listener.set=set_viewport;
    init_display(&view->display);
    init_accumulator(view);
    float eye[16] = {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1
    };
    subvshow_set_transform(self,eye);

    rotatable_cube_init(&self->rotatable_cube,(int[]){0,0,0},(int[]){64,64,64});    

    listener_connect(
        &self->rotatable_listener,
        &self->rotatable_cube.rotatable.observable,
        0,
        on_rotatable
    );
    view->is_dirty=1;
}

// 
// Subvolume accumulation
// 

// 1. Set the bounding volume shape so we know how to scale subvolumes
// 2. Clear max accumulator 
// 3. Bind the right texture to the fbo
void subvshow_beg(struct subvshow *self, const struct nd *shape) {    
    struct subvshow_view* view=&self->view;
    view->is_dirty=0;
    switch(shape->ndim) {
        case 0:   memcpy(view->extent,(float[3]){1,1,1}                                                            ,sizeof(view->extent)); break;
        case 1:   memcpy(view->extent,(float[3]){(float)shape->dims[0],1,1}                                        ,sizeof(view->extent)); break;
        case 2:   memcpy(view->extent,(float[3]){(float)shape->dims[0],(float)shape->dims[1],1}                    ,sizeof(view->extent)); break;
        default:  memcpy(view->extent,(float[3]){(float)shape->dims[0],(float)shape->dims[1],(float)shape->dims[2]},sizeof(view->extent));
    }

    GLint type,internal;
    switch(shape->type) {
        case nd_i8:  internal=GL_R8;        type=GL_BYTE;           break;
        case nd_u8:  internal=GL_R8_SNORM;  type=GL_UNSIGNED_BYTE;  break;
        case nd_i16: internal=GL_R16_SNORM; type=GL_SHORT;          break;
        case nd_u16: internal=GL_R16;       type=GL_UNSIGNED_SHORT; break;
        case nd_i32: internal=GL_R32I;      type=GL_INT;            break;
        case nd_u32: internal=GL_R32UI;     type=GL_UNSIGNED_INT;   break;
        case nd_f32: internal=GL_RED;       type=GL_FLOAT;          break;
        default:
            return;//ERR("Unsupported type for texture.\n");
    }
    view->mx.type=type;
    view->mx.internal=internal;

    glViewport(0,0,view->w,view->h);
    // set accumulator texture type
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,view->mx.acc);
    glTexImage2D(GL_TEXTURE_2D,0,view->mx.internal,view->w,view->h,0,GL_RED,view->mx.type,0);
    // clear fbo for max accumulation
    glBindFramebuffer(GL_FRAMEBUFFER,view->fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, view->mx.acc, 0);
    glClearColor(0,0,1,0);
    glClear(GL_COLOR_BUFFER_BIT);
    // Set bindings for max accumulation
    glUseProgram(view->mx.program);
    glBindFramebuffer(GL_FRAMEBUFFER,view->fbo);
    glUniform1f(view->mx.id.counts,view->mx.counts++);    
    glBindVertexArray(view->vao);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        OutputDebugStringA("ruh roh!\n"); // FIXME: need better error handling
        return;
    }
}

// 1. Translate and scale subvolume
// 2. Max accumulation
// 
// Note: How best to position subvolumes?  I think the most direct approach
//       is to scale/translate the transform used during ray casting.
//       An alternative might be to just update the vertex array used to
//       generate uv's, but I suspect that's more complicated.
void subvshow_add(struct subvshow *self, const float *offset, const struct nd *shape, const void *data) {
    struct subvshow_view *view=&self->view;
    struct subvshow_max_accumulator *mx=&view->mx;
    GLsizei w=(GLsizei)shape->dims[0],
            h=(GLsizei)shape->dims[1],
            d=(GLsizei)shape->dims[2];
    
    GLint type,internal;
    switch(shape->type) {
        case nd_i8:  internal=GL_R8;        type=GL_BYTE;           break;
        case nd_u8:  internal=GL_R8_SNORM;  type=GL_UNSIGNED_BYTE;  break;
        case nd_i16: internal=GL_R16_SNORM; type=GL_SHORT;          break;
        case nd_u16: internal=GL_R16;       type=GL_UNSIGNED_SHORT; break;
        case nd_i32: internal=GL_R32I;      type=GL_INT;            break;
        case nd_u32: internal=GL_R32UI;     type=GL_UNSIGNED_INT;   break;
        case nd_f32: internal=GL_RED;       type=GL_FLOAT;          break;
        default:
            return;//ERR("Unsupported type for texture.\n");
    }

    {   // compute transform.
        float s[3]={view->extent[0]/(float)w,
                    view->extent[1]/(float)h,
                    view->extent[2]/(float)d  };
        // TODO: figure out whether I should be doing this in 
        //       row-major or col-major
        float T[16];
        float m[16]={ // inverse scaling and translation of subvolume into extent
            s[0],0.0f,0.0f, -offset[0]/(float)w,
            0.0f,s[1],0.0f, -offset[1]/(float)h,
            0.0f,0.0f,s[2], -offset[2]/(float)d,
            0.0f,0.0f,0.0f,  1.0f};
        mul44(T,m,self->T);
        glUniformMatrix4fv(mx->id.T,1,GL_TRUE,T);
    }

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D,mx->vol);
    glTexImage3D (GL_TEXTURE_3D,0,internal,w,h,d,0,GL_RED,type,data);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    
    glBindTexture(GL_TEXTURE_3D,0);
    mingl_check();
}

// 1. Mean accumulation
// 2. Clean up
void subvshow_end(struct subvshow *self) {
    struct subvshow_view *view=&self->view;
    struct subvshow_mean_accumulator *mean=&view->mean;
    mean->counts++;

    // Set bindings for mean accumulation

    glUseProgram(mean->program);
    glUniform1f(mean->id.norm,1.0f/mean->counts);
    glBindFramebuffer(GL_FRAMEBUFFER,view->fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mean->acc, 0);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,mean->acc);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,view->mx.acc);
    glBindVertexArray(view->vao);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    glBindTexture(GL_TEXTURE_2D,0);
    glBindVertexArray(0);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    glUseProgram(0);
    glViewport(0,0,(GLsizei)view->display.w,(GLsizei)view->display.h);
    mingl_check();
}

static void display(struct subvshow_view *view) {
    const struct subvshow_display *display=&view->display;
    const struct subvshow_mean_accumulator *mean=&view->mean;
    glViewport(0,0,(GLsizei)display->w,(GLsizei)display->h);
    glUseProgram(display->program);

    glActiveTexture(GL_TEXTURE1);
    // glBindTexture(GL_TEXTURE_2D,view->mx.acc);
    glBindTexture(GL_TEXTURE_2D,mean->acc);
    glBindVertexArray(display->vao);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    glBindTexture(GL_TEXTURE_2D,0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);

    mingl_check();
}

void subvshow_draw(struct subvshow *self) {
    display(&self->view);
}

static void swap(struct subvshow_view* view) {
    view->is_dirty=1;
    struct subvshow_mean_accumulator *mean=&view->mean;
    mean->counts=0;
    // clear fbo
    glBindFramebuffer(GL_FRAMEBUFFER,view->fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mean->acc, 0);
    glViewport(0,0,view->w,view->h);
    glClearColor(0,1,1,0);
    glClear(GL_COLOR_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    // reset viewport
    glViewport(0,0,(GLsizei)view->display.w,(GLsizei)view->display.h);
}

void subvshow_contrast(struct subvshow *self,enum nd_type type,float min,float max){
    struct subvshow_display *ctx=&self->view.display;
    float scale=1.0f;

    // check to see if anything changed before proceeding
    if(ctx->min==min && ctx->max==max) 
        return;
    ctx->min=min;
    ctx->max=max;

    switch(type) {
        case nd_i8:  scale=(float)(1<<7); break;
        case nd_u8:  scale=(float)(1<<8); break;
        case nd_i16: scale=(float)(1<<15); break;
        case nd_u16: scale=(float)(1<<16); break;
        case nd_i32: scale=(float)(1<<31); break;
        case nd_u32: scale=(float)(1ULL<<32); break;
        default:;
    }
    glUseProgram(ctx->program);
    glUniform1f(ctx->id.zero,(GLfloat)min/scale);
    glUniform1f(ctx->id.range,(GLfloat)(max-min)/scale);
    glUseProgram(0);
}

void subvshow_set_transform(struct subvshow *self,const float *mat44){
    if(!is_equal_mat44(self->T,mat44,1.0e-3f)) {
        memcpy(self->T,mat44,sizeof(self->T));
        swap(&self->view);
    }
}

void subvshow_set_view_plane(struct subvshow *self,float z,float dz) {
    struct subvshow_max_accumulator *accumulator=&self->view.mx;

    // shader wants z,dz in unit coordinates
    glUseProgram(accumulator->program);
    glUniform1f(accumulator->id.z,z);
    glUniform1f(accumulator->id.dz,dz);
    glUseProgram(0);

    if(z!=accumulator->z || dz!=accumulator->dz) {
        accumulator->z=z;
        accumulator->dz=dz;
        swap(&self->view);
    }
}

void subvshow_rotate_about_center(struct subvshow *self,const float *mat33) {
    float R[16]={[15]=1.0f},
          T[16]={1,0,0,-0.5, 0,1,0,-0.5, 0,0,1,-0.5, 0,0,0,1},
       invT[16]={1,0,0, 0.5, 0,1,0, 0.5, 0,0,1, 0.5, 0,0,0,1},
        tmp[16];
    for(int c=0;c<3;c++)
        for(int r=0;r<3;r++)
            R[4*c+r]=mat33[3*c+r];

    mul44(tmp    ,R   ,T);
    mul44(T      ,invT,tmp);
    subvshow_set_transform(self,T);

    memcpy(self->rotatable_cube.rotatable.mat,mat33,9*sizeof(float));
}

static void update_rotatable_rect(struct rotatable_cube *cube,float x0, float y0, float w, float h) {
    int ori[]={x0,y0,w/2.0f},dims[]={w,h,w};
    rotatable_cube_set(cube,ori,dims);
}

void subvshow_rect(struct subvshow *self,float x0,float y0,float w,float h){
    struct subvshow_display *display=&self->view.display;
    const float
        x1=x0+w,
        y1=y0+h,
        u0=0.0f,v0=0.0f,u1=1.0f,v1=1.0f;
    struct verts verts[4]={
        {x0,y0,u0,v0},
        {x0,y1,u0,v1},
        {x1,y1,u1,v1},
        {x1,y0,u1,v0},
    };    
    glUseProgram(display->program);
    glBindBuffer(GL_ARRAY_BUFFER,display->vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);

    struct subvshow_view *view=&self->view;  
    view->w=(unsigned)w;
    view->h=(unsigned)h;    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,view->mx.acc);
    glTexImage2D(GL_TEXTURE_2D,0,view->mx.internal,view->w,view->h,0,GL_RED,view->mx.type,0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,view->mean.acc);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,view->w,view->h,0,GL_RGB,GL_FLOAT,0);

    glUseProgram(view->mx.program);
    glUniform1f(view->mx.id.size,(float)view->w);
    glUseProgram(0);

    swap(view);

    update_rotatable_rect(&self->rotatable_cube,x0,y0,w,h);
}

void subvshow_clear(struct subvshow *self) {
    swap(&self->view);
}