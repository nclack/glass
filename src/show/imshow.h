#pragma once
#ifndef H_NGC_IMAGE
#define H_NGC_IMAGE

#include "../tiff-metadata-extractor/src/nd.h"

#ifdef __cplusplus
#extern "C" {
#endif

    void imshow(const struct nd *shape,const void *data);
    void imshow_contrast(enum nd_type type,float min,float max);
    void imshow_rect(float x,float y,float w,float h);
    void imshow_viewport(int w,int h);
    

#ifdef __cplusplus
}
#endif

#endif


/* NOTES
 * 
 * If I need to have more than one image display, this will 
 * have to change a bit. (pretty sure)
 */