#pragma once
#ifndef H_NGC_SUBVSHOW
#define H_NGC_SUBVSHOW

#include "../tiff-metadata-extractor/src/nd.h"
#include "../controls/behavior/viewport.h"
#include "../controls/behavior/rotatable.h"

#ifdef __cplusplus
#extern "C" {
#endif

    struct subvshow {
        struct subvshow_view {
            unsigned w,h;               // (w,h) of accumulator in px.  Rendering at this resolution.
            unsigned fbo,vbo,vao;
            float extent[3];

            struct subvshow_display {
                unsigned program,vbo,vao;
                float w,h;           // width and height of display in pixels.  Rendering will get stretched (scaled) to fill the display rect.
                float min,max;       // last values used to set contrast
                struct subvshow_view_uniforms {
                    unsigned
                            zero,range, // intensity scaling
                            size;      // viewport size. used for pixel scale
                } id;
            } display;

            struct subvshow_max_accumulator {
                unsigned program;
                unsigned vol,acc;   // textures: volume (3d,TEXTURE1) and accumulator (2d,TEXTURE0)
                float z,dz;         // last plane and depth-of-field in 0 to 1 coordinates
                float counts;
                int type,internal;  // format types used for accumulator texture
                struct subvshow_max_accumulator_uniforms {
                    unsigned
                        counts,     // used for hash function
                        size,       // width of the field (to determine pixel size)
                        T,          // 3d affine transform
                        z,dz;       // view plane, depth of field
                } id;            
            } mx;

            struct subvshow_mean_accumulator {
                    unsigned program;
                    unsigned acc;       // texture (2d: TEXTURE1).  The max accumulator will be left on TEXTURE0.
                    unsigned counts;
                    struct subvshow_mean_accumulator_uniforms {
                        unsigned 
                            norm;       // 1/number of accumulations that have run since the last clear
                } id;
            } mean;

            struct viewport_listener viewport_listener;
            int is_dirty;
        } view;
        struct rotatable_cube rotatable_cube;
        struct listener rotatable_listener;
        const float T[16];
        const char *log;        // last error message (if any)
        float last_vol_texture_load_time_s;
    };

    void subvshow_init             (struct subvshow *self);
    void subvshow_draw             (struct subvshow *self);
    void subvshow_contrast         (struct subvshow *self,enum nd_type type,float min,float max);
    void subvshow_set_view_plane   (struct subvshow *self,float z,float dz); // 0 to 1
    void subvshow_set_transform    (struct subvshow *self,const float *mat44);
    void subvshow_rotate_about_center(struct subvshow *self,const float *mat33);
    void subvshow_rect             (struct subvshow *self,float x,float y,float w,float h);

    void subvshow_beg              (struct subvshow *self, const struct nd *shape);
    void subvshow_add              (struct subvshow *self, const float *offset, const struct nd *shape, const void *data);
    void subvshow_end              (struct subvshow *self);

    void subvshow_clear            (struct subvshow *self);
#ifdef __cplusplus
}
#endif

#endif

/* NOTES

Usage

    Each frame, the render for a full volume is accomplished by composing several subvolumes.
    So the draw call for each frame should look something like:

    ondraw () {
        subvshow_beg(subvshow,shape_of_full_volume);          // the shape of the full volume sets up the expected geometry
        foreach(v in subvolumes) {
            subvshow_add(subvshow,v.offset,v.shape,v.data);   // uploads the 3d texture and does max projection sampling
        } 
        subvshow_end(subvshow);                               // commits the max projection buffer to another buffer for accumulating the means
    }



Performance

    "show" is separated from "draw" to avoid updating the 3d texture on every frame.

Transform

    uvw_out = T * uv_in.

    Another way of putting that is:

        uvw_out[i]=dot(T[row i],uv_in)

    T is in column major order; adjacent elements in memory are moving from one row to the next.
    
    uv_in is in an input space, where coefficients range from 0 to 1.  These represent the 
    extents of the view rectangle.

    uv_out represents the 3d sampling point in the volume.  Points inside the volume have
    coefficients between 0 and 1.

    T is affine.  Let T=[A|b] where A is the 3x3 upper-left scaling/rotation/shear matrix, and
    b is the translation part.  Then Tv' = Av+b where v is a 3d vector. v' is the augmented form
    of v (for the affine).  The translation, b, is in the output coordinate space.



*/