#include "../pixelfont/src/mingl.h"
#include "../tiff-metadata-extractor/src/nd.h"

static const char FRAG[]=GLSL(
    in  vec2 tex_;
    out vec4 color;
    uniform sampler2D im;
    uniform float zero,range;

    void main(){
        vec4 c=texture(im,tex_);
        c.r=(c.r-zero)/range;
        color=vec4(c.r,c.r,c.r,c.r);
    }
);

static const char VERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    uniform vec2 size;
    out vec2 tex_;

    void main(){
        gl_Position=vec4(2*(vert/size)-1,0,1.0f);
        gl_Position.y=-gl_Position.y;
        tex_=tex;
    }
);

struct verts{ float x,y,u,v; };

static struct image {
    unsigned tex,program,vbo,vao;
    struct ids{
        int size,zero,range;
    } id;
} IMAGE, *image_;

/// Inits the image display
static void init() {
    struct image *image=&IMAGE;
    // Shader 
    image->program=mingl_new_program(FRAG,sizeof(FRAG),VERT,sizeof(VERT),0,0);
    image->id.size =glGetUniformLocation(image->program,"size");
    image->id.zero =glGetUniformLocation(image->program,"zero");
    image->id.range=glGetUniformLocation(image->program,"range");
    glUseProgram(image->program);
    glUniform1i(glGetUniformLocation(image->program,"im"),0);
    {
        GLint sz[4]={0};
        glGetIntegerv(GL_VIEWPORT,sz);
        glUniform2f(image->id.size,(GLfloat)sz[2],(GLfloat)sz[3]);
    }
    glUseProgram(0);

    // Texturing
    glGenTextures(1,&image->tex);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,image->tex);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_BORDER);
    glBindTexture(GL_TEXTURE_2D,0);

    // Prepping vertex buffers
    glGenBuffers(1,&image->vbo);
    glGenVertexArrays(1,&image->vao);
    glBindVertexArray(image->vao);
    glBindBuffer(GL_ARRAY_BUFFER,image->vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
    glBindVertexArray(0);

    if(!mingl_check())
        goto OpenGLError;
    image_=image;
    return;
OpenGLError:
    image_=0;
}

// TODO: the verts we want to draw are pretty static
static void draw(struct image* image) {
    glUseProgram(image->program);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,image->tex);
    glBindBuffer(GL_ARRAY_BUFFER,image->vbo);
    glBindVertexArray(image->vao);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    glBindTexture(GL_TEXTURE_2D,0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);
}



void imshow(const struct nd *shape,const void *data) {
    GLsizei w=shape->dims[0],
            h=shape->dims[1];
    GLint type;
    switch(shape->type) {
        case nd_i8:  type=GL_BYTE; break;
        case nd_u8:  type=GL_UNSIGNED_BYTE; break;
        case nd_i16: type=GL_SHORT; break;
        case nd_u16: type=GL_UNSIGNED_SHORT; break;
        case nd_i32: type=GL_INT; break;
        case nd_u32: type=GL_UNSIGNED_INT; break;
        case nd_f32: type=GL_FLOAT; break;
        default:
            return;//ERR("Unsupported type for texture.\n");
    }
    glBindTexture(GL_TEXTURE_2D,image_->tex);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RED,w,h,0,GL_RED,type,data);
    glBindTexture(GL_TEXTURE_2D,0);

    mingl_check();
    draw(image_);
}

void imshow_rect(float x0, float y0, float w, float h) {
    if(!image_) init();
    const float
        x1=x0+w,
        y1=y0+h,
        u0=0.0f,v0=0.0f,u1=1.0f,v1=1.0f;
    struct verts verts[4]={
        {x0,y0,u0,v0},
        {x0,y1,u0,v1},
        {x1,y1,u1,v1},
        {x1,y0,u1,v0},
    };    
    glUseProgram(image_->program);
    glBindBuffer(GL_ARRAY_BUFFER,image_->vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);
}

void imshow_viewport(int w,int h) {
    if(!image_) init();
    glUseProgram(image_->program);
    glUniform2f(image_->id.size,(GLfloat)w,(GLfloat)h);
    glUseProgram(0);
}

void imshow_contrast(enum nd_type type, float min,float max) {
    if(!image_) init();
    float scale=1.0f;
    switch(type) {
        case nd_i8:  scale=(float)(1<<7); break;
        case nd_u8:  scale=(float)(1<<8); break;
        case nd_i16: scale=(float)(1<<15); break;
        case nd_u16: scale=(float)(1<<16); break;
        case nd_i32: scale=(float)(1<<31); break;
        case nd_u32: scale=(float)(1<<32); break;        
        default:;
    }
    glUseProgram(image_->program);
    glUniform1f(image_->id.zero,(GLfloat)min/scale);
    glUniform1f(image_->id.range,(GLfloat)(max-min)/scale);
    glUseProgram(0);
}


/*
 * NOTES
 *
 * I'm going to start out implementing a single image display, but my guess
 * is that I'll need to have multiple image displays before long.
 *
 * I'll also start out with forcing a 1:1 pixel aspect ratio and using nearest,
 * but maybe later I'll use different aspects and samplers.
 */
