#pragma once
#ifndef H_NGC_VSHOW
#define H_NGC_VSHOW

#include "../tiff-metadata-extractor/src/nd.h"
#include "../controls/behavior/viewport.h"
#include "../controls/behavior/rotatable.h"

#ifdef __cplusplus
#extern "C" {
#endif

    struct vshow {
        struct vshow_view {
            struct vshow_display {
                unsigned program,vbo,vao;
                unsigned w,h;
                struct vshow_view_uniforms {
                    unsigned                     
                        size;       // viewport size. used for pixel scale
                } id;
            } display;
            struct vshow_accumulator {
                unsigned program,fbo,vbo,vao;
                unsigned vol,acc;   // textures: volume (3d,TEXTURE1) and accumulator (2d,TEXTURE0)
                unsigned counts;
                unsigned w,h;       // (w,h) of accumulator in px
                float z,dz;         // last plane and depth-of-field in 0 to 1 coordinates
                float min,max;      // last values used to set contrast
                struct vshow_accumulator_uniforms {
                    unsigned 
                        zero,range, // intensity scaling
                        counts,     // number of accumulations that have run since the last clear
                        size,       // width of the field (to determine pixel size)
                        T,          // 3d affine transform
                        z,dz;       // view plane, depth of field
                } id;
            } accumulator;
            struct viewport_listener viewport_listener;
        } view;        
        struct rotatable_cube rotatable_cube;
        struct listener rotatable_listener;
        const float T[16];
        const char *log;        // last error message (if any)
        float last_vol_texture_load_time_s;
    };

    void vshow_init             (struct vshow *self);
    void vshow_show             (struct vshow *self, const struct nd *shape,const void *data);
    void vshow_draw             (struct vshow *self);
    void vshow_contrast         (struct vshow *self,enum nd_type type,float min,float max);
    void vshow_set_view_plane   (struct vshow *self,float z,float dz); // 0 to 1
    void vshow_set_transform    (struct vshow *self,const float *mat44);
    void vshow_rotate_about_center(struct vshow *self,const float *mat33);
    void vshow_rect             (struct vshow *self,float x,float y,float w,float h);    

#ifdef __cplusplus
}
#endif

#endif

/* NOTES

Performance

    "show" is separated from "draw" to avoid updating the 3d texture on every frame.

Transform

    uvw_out = T * uv_in.

    Another way of putting that is:

        uvw_out[i]=dot(T[row i],uv_in)

    T is in column major order; adjacent elements in memory are moving from one row to the next.
    
    uv_in is in an input space, where coefficients range from 0 to 1.  These represent the 
    extents of the view rectangle.

    uv_out represents the 3d sampling point in the volume.  Points inside the volume have
    coefficients between 0 and 1.

    T is affine.  Let T=[A|b] where A is the 3x3 upper-left scaling/rotation/shear matrix, and
    b is the translation part.  Then Tv' = Av+b where v is a 3d vector. v' is the augmented form
    of v (for the affine).  The translation, b, is in the output coordinate space.



*/