#include "vshow.h"
#include "../pixelfont/src/mingl.h"
#include <math.h> // fabsf

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct verts {float x,y,u,v;};

static void swap(struct accumulator* ctx);

static const char FRAG[]=GLSL(
    in  vec2 uv;
    out vec4 color;
    uniform sampler2D im;

    void main(){
        color=texture(im,uv);
    }
);

static const char VERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    uniform vec2 size;
    out vec2 uv;

    void main(){
        gl_Position=vec4(2*(vert/size)-1,0,1.0f);
        gl_Position.y=-gl_Position.y;
        uv=tex;
    }
);

static const char MCFRAG[]=GLSL(
    in  vec2 uv;
    out vec4 color;
    uniform sampler2D accumulator;
    uniform sampler3D vol;
    uniform mat4 T; // T[c][r]
    uniform float size;
    uniform float zero,range,z;
    uniform float dz;
    uniform float counts;

    // hash functions derived from
    // "Hash without Sine"
    // https://www.shadertoy.com/view/4djSRW
    // returns something between (-0.5,0.5)

    float hash11(float p)
    {
        vec3 p3  = fract(vec3(p) * 443.8975);
        p3 += dot(p3, p3.yzx + 19.19);
        return fract((p3.x + p3.y) * p3.z)-0.5f;
    }

    //----------------------------------------------------------------------------------------
    ///  2 out, 3 in...
    vec2 hash23(vec3 p3)
    {
        p3 = fract(p3 * vec3(443.897, 441.423, 437.195));
        p3 += dot(p3, p3.yzx+19.19);
        return fract((p3.xx+p3.yz)*p3.zy)-0.5f;
    }

    vec3 hash33(vec3 p3)
    {
        p3 = fract(p3 *  vec3(443.897, 441.423, 437.195));
        p3 += dot(p3, p3.yxz+19.19);
        return fract((p3.xxy + p3.yxx)*p3.zyx)-0.5; 
    }

    // NITER: 
    // Max projection over NITER random samples
    // Each frame, averages the result
    // Adjusting NITER tunes between mean projection (NITER=1) and max projection (say NITER~=100).
    const int NITER=100;
    void main() {
        // color=vec4(hash33(vec3(uv.xy,counts))+0.5,1);
        // return;

        vec4 v=texture(accumulator,uv);
        float vv=0.0; //gl clamps input negative valiues to zero anyway

        vec2 dxy=hash23(vec3(uv.xy,counts*0.01))/size;
        for(int i=0;i<NITER;++i) {
            float pz=hash11((counts*10+i)*0.01);
            vec4 s=T*vec4(uv+dxy,z+(pz+0.5)*dz,1);
            vec4 c=texture(vol,s.xyz);
            vv=max(vv,c.r);
        }
        vv=(vv-zero)/range;
        //color=vec4(vv,vv,vv,1);
        color=mix(v,vec4(vv,vv,vv,1),1.0/counts); //color=mix(v,vv/float(NITER),1.0/counts);
    }
);

static const char MCVERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    out vec2 uv;

    void main(){
        gl_Position=vec4(vert,0,1.0f);
        uv=tex;
    }
);


// implementation

static void init_display(struct vshow_display *self) {
    // Shader 
    self->program=mingl_new_program(FRAG,sizeof(FRAG),VERT,sizeof(VERT),0,0);
    self->id=(struct vshow_view_uniforms) {
        .size =glGetUniformLocation(self->program,"size"),
    };
    glUseProgram(self->program);
    glUniform1i(glGetUniformLocation(self->program,"tex"),0);
    {
        GLint sz[4]={0};
        glGetIntegerv(GL_VIEWPORT,sz);
        glUniform2f(self->id.size,(GLfloat)sz[2],(GLfloat)sz[3]);
    }
    glUseProgram(0);

    // Prepping vertex buffers
    glGenBuffers(1,&self->vbo);
    glGenVertexArrays(1,&self->vao);
    glBindVertexArray(self->vao);
    glBindBuffer(GL_ARRAY_BUFFER,self->vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
    glBindVertexArray(0);

    if(!mingl_check())
        DebugBreak();
}
            
static void init_accumulator(struct vshow_accumulator* ctx) {
    ctx->counts=0;
    ctx->w=ctx->h=50;
    ctx->program=mingl_new_program(MCFRAG,sizeof(MCFRAG),MCVERT,sizeof(MCVERT),0,0);
    ctx->id=(struct vshow_accumulator_uniforms) {
        .zero=glGetUniformLocation(ctx->program,"zero"),
        .range=glGetUniformLocation(ctx->program,"range"),
        .counts=glGetUniformLocation(ctx->program,"counts"),
        .size=glGetUniformLocation(ctx->program,"size"),
        .T=glGetUniformLocation(ctx->program,"T"),
        .z=glGetUniformLocation(ctx->program,"z"),
        .dz=glGetUniformLocation(ctx->program,"dz"),
    };
    glUseProgram(ctx->program);
    glUniform1i(glGetUniformLocation(ctx->program,"vol"),1);
    glUniform1i(glGetUniformLocation(ctx->program,"acc"),0);
    glUniform1f(ctx->id.size,(float)ctx->w);
    glUseProgram(0);

    glGenFramebuffers(1,&ctx->fbo);
    
    glGenTextures(1,&ctx->acc);
    glBindTexture(GL_TEXTURE_2D,ctx->acc);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,ctx->w,ctx->h,0,GL_RGB,GL_FLOAT,0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glGenTextures(1,&ctx->vol);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture  (GL_TEXTURE_3D,ctx->vol);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_3D,GL_TEXTURE_WRAP_R,GL_CLAMP_TO_BORDER);
    glBindTexture  (GL_TEXTURE_3D,0);

    glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ctx->acc, 0);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        OutputDebugStringA("ruh roh!\n"); // FIXME: need better error handling
        return;
    }

    { 
        // prep vertex buffer for rendering to texture 
        glGenBuffers(1,&ctx->vbo);
        glGenVertexArrays(1,&ctx->vao);
        glBindVertexArray(ctx->vao);
        glActiveTexture(GL_TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
        glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
        glBindVertexArray(0);

        const float 
            x0=-1.0f,y0=-1.0f,x1=1.0f,y1=1.0f,
            u0= 0.0f,v0= 0.0f,u1=1.0f,v1=1.0f;
        struct verts verts[4]={
            {x0,y0,u0,v0},
            {x0,y1,u0,v1},
            {x1,y1,u1,v1},
            {x1,y0,u1,v0},
        };        
        glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
        glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        
    }
}

static void set_viewport(struct viewport_listener *self,float w,float h){
    struct vshow_view *view=containerof(self,struct vshow_view,viewport_listener);
    struct vshow_display *display=&view->display;
    display->w=w;
    display->h=h;
    glUseProgram(display->program);
    glUniform2f(display->id.size,(GLfloat)w,(GLfloat)h);
    glUseProgram(0);
}

static void on_rotatable(struct listener* listener) {
    struct rotatable *source=containerof(listener->source,struct rotatable,observable);
    struct vshow *sink=containerof(listener,struct vshow,rotatable_listener);
    vshow_rotate_about_center(sink,source->mat);
}

void vshow_init(struct vshow *self){
    struct vshow_view *view=&self->view;
    view->viewport_listener.set=set_viewport;
    init_display(&view->display);
    init_accumulator(&view->accumulator);
    float eye[16] = {
        1,0,0,0,
        0,1,0,0,
        0,0,1,0,
        0,0,0,1
    };
    vshow_set_transform(self,eye);

    rotatable_cube_init(&self->rotatable_cube,(int[]){0,0,0},(int[]){64,64,64});    

    listener_connect(
        &self->rotatable_listener,
        &self->rotatable_cube.rotatable.observable,
        0,
        on_rotatable
    );
}

static void vshow_update_volume(struct vshow_accumulator *self, const struct nd *shape,const void *data) {
    GLsizei w=shape->dims[0],
            h=shape->dims[1],
            d=shape->dims[2];
    
    GLint type,internal;
    switch(shape->type) {
        case nd_i8:  internal=GL_R8; type=GL_BYTE; break;
        case nd_u8:  internal=GL_R8_SNORM; type=GL_UNSIGNED_BYTE; break;
        case nd_i16: internal=GL_R16_SNORM; type=GL_SHORT; break;
        case nd_u16: internal=GL_R16; type=GL_UNSIGNED_SHORT; break;
        case nd_i32: internal=GL_R32I; type=GL_INT; break;
        case nd_u32: internal=GL_R32UI; type=GL_UNSIGNED_INT; break;
        case nd_f32: internal=GL_RED; type=GL_FLOAT; break;
        default:
            return;//ERR("Unsupported type for texture.\n");
    }
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D,self->vol);

    glTexImage3D (GL_TEXTURE_3D,0,internal,w,h,d,0,GL_RED,type,data);

    glBindTexture(GL_TEXTURE_3D,0);
    mingl_check();

    swap(self);
}

void vshow_show(struct vshow *self, const struct nd *shape,const void *data) {
    vshow_update_volume(&self->view.accumulator,shape,data);
    glViewport(0,0,self->view.display.w,self->view.display.h);
}

static void accumulate(struct vshow_accumulator* ctx) {
    ++ctx->counts;
    // render to fbo
    glViewport(0,0,ctx->w,ctx->h);
    glUseProgram(ctx->program);
    glUniform1f(ctx->id.counts,(float)ctx->counts);
    glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,ctx->acc);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_3D,ctx->vol);
    // glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
    glBindVertexArray(ctx->vao);
    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    glBindFramebuffer(GL_FRAMEBUFFER,0);

    mingl_check();
}

static void display(struct vshow_view *view) {
    const struct vshow_display *display=&view->display;
    const struct vshow_accumulator *accumulator=&view->accumulator;
    glViewport(0,0,display->w,display->h);
    glUseProgram(display->program);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,accumulator->acc);
    glBindVertexArray(display->vao);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);

    glBindTexture(GL_TEXTURE_2D,0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);

    mingl_check();
}

void vshow_draw(struct vshow *self) {
    accumulate(&self->view.accumulator);
    display(&self->view);
}

static void swap(struct vshow_accumulator* ctx) {
    ctx->counts=0;
    // clear fbo
    glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ctx->acc, 0);
    glViewport(0,0,ctx->w,ctx->h);
    glClearColor(0,0,1,0);
    glClear(GL_COLOR_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
}


void vshow_contrast(struct vshow *self,enum nd_type type,float min,float max){
    struct vshow_accumulator *accumulator=&self->view.accumulator;
    float scale=1.0f;

    // check to see if anything changed before proceeding
    if(accumulator->min==min && accumulator->max==max) 
        return;
    accumulator->min=min;
    accumulator->max=max;

    switch(type) {
        case nd_i8:  scale=(float)(1<<7); break;
        case nd_u8:  scale=(float)(1<<8); break;
        case nd_i16: scale=(float)(1<<15); break;
        case nd_u16: scale=(float)(1<<16); break;
        case nd_i32: scale=(float)(1<<31); break;
        case nd_u32: scale=(float)(1<<32); break;
        default:;
    }
    glUseProgram(accumulator->program);
    glUniform1f(accumulator->id.zero,(GLfloat)min/scale);
    glUniform1f(accumulator->id.range,(GLfloat)(max-min)/scale);
    glUseProgram(0);
 
    swap(&self->view.accumulator);
    glViewport(0,0,self->view.display.w,self->view.display.h);
}

static int is_equal_mat44(const float *a,const float *b, const float eps) {
    if(a==b) 
        return 1;
    for(int i=0;i<16;++i) 
        if(fabsf(a[i]-b[i])>eps) 
            return 0;
    return 1;
}

void vshow_set_transform(struct vshow *self,const float *mat44){
    struct vshow_accumulator *accumulator=&self->view.accumulator;
    glUseProgram(accumulator->program);
    // To transpose or not to transpose:
    //  GL_FALSE: mat44 column-major
    //  GL_TRUE : mat44 row-major
    glUniformMatrix4fv(accumulator->id.T,1,GL_TRUE,mat44);
    glUseProgram(0);

    if(!is_equal_mat44(self->T,mat44,1e-3)) {
        memcpy(self->T,mat44,sizeof(self->T));
        swap(accumulator);
        glViewport(0,0,self->view.display.w,self->view.display.h);
    }
}

void vshow_set_view_plane(struct vshow *self,float z,float dz) {
    struct vshow_accumulator *accumulator=&self->view.accumulator;

    // shader wants z,dz in unit coordinates
    glUseProgram(accumulator->program);
    glUniform1f(accumulator->id.z,z);
    glUniform1f(accumulator->id.dz,dz);
    glUseProgram(0);

    if(z!=accumulator->z || dz!=accumulator->dz) {
        accumulator->z=z;
        accumulator->dz=dz;
        swap(accumulator);
        glViewport(0,0,self->view.display.w,self->view.display.h);
    }
}

static void mul44(float* out, const float *a, const float *b) {
    for(int c=0;c<4;++c) {
        for(int r=0;r<4;++r) {
            float acc=0.0f;
            for(int i=0;i<4;++i)
                acc+=a[i+4*r]*b[c+4*i];
            out[c+4*r]=acc;
        }
    }
}

void vshow_rotate_about_center(struct vshow *self,const float *mat33) {
    float R[16]={[15]=1.0f},
          T[16]={1,0,0,-0.5, 0,1,0,-0.5, 0,0,1,-0.5, 0,0,0,1},
       invT[16]={1,0,0, 0.5, 0,1,0, 0.5, 0,0,1, 0.5, 0,0,0,1},
        tmp[16],tmp2[16];
    for(int c=0;c<3;c++)
        for(int r=0;r<3;r++)
            R[4*c+r]=mat33[3*c+r];

    mul44(tmp    ,R   ,T);
    mul44(T      ,invT,tmp);
    vshow_set_transform(self,T);

    memcpy(self->rotatable_cube.rotatable.mat,mat33,9*sizeof(float));
}

static void update_rotatable_rect(struct rotatable_cube *cube,float x0, float y0, float w, float h) {
    int ori[]={x0,y0,w/2},dims[]={w,h,w};
    rotatable_cube_set(cube,ori,dims);
}

void vshow_rect(struct vshow *self,float x0,float y0,float w,float h){
    struct vshow_display *display=&self->view.display;
    const float
        x1=x0+w,
        y1=y0+h,
        u0=0.0f,v0=0.0f,u1=1.0f,v1=1.0f;
    struct verts verts[4]={
        {x0,y0,u0,v0},
        {x0,y1,u0,v1},
        {x1,y1,u1,v1},
        {x1,y0,u1,v0},
    };    
    glUseProgram(display->program);
    glBindBuffer(GL_ARRAY_BUFFER,display->vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);

    struct vshow_accumulator *accumulator=&self->view.accumulator;  
    accumulator->w=(unsigned)w;
    accumulator->h=(unsigned)h;
    
    glBindTexture(GL_TEXTURE_2D,accumulator->acc);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,accumulator->w,accumulator->h,0,GL_RGB,GL_FLOAT,0);
    glUseProgram(accumulator->program);
    glUniform1f(accumulator->id.size,(float)accumulator->w);
    glUseProgram(0);

    swap(accumulator);
    glViewport(0,0,display->w,display->h); // restore viewport

    update_rotatable_rect(&self->rotatable_cube,x0,y0,w,h);
}

