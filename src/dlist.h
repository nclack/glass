#pragma once
#ifndef H_NGC_DLIST
#define H_NGC_DLIST

#ifdef __cplusplus
extern "C" {
#endif

    struct dlist_item {
        struct dlist_item *prev,*next;
    };

    struct dlist {
        struct dlist_item sentinal;
        struct dlist_item *cur;
    };
        
    struct dlist_iterable {
        struct dlist *list;
        struct dlist_item *cur;
    };

    void dlist_init(struct dlist* dlist);
    void dlist_insert(struct dlist* dlist,struct dlist_item *el);
    void dlist_remove(struct dlist* dlist,struct dlist_item *el);
    int  dlist_is_empty(const struct dlist *dlist);

    void dlist_beg(struct dlist_iterable *it,struct dlist* list);
    void dlist_next(struct dlist_iterable *it);
    int  dlist_is_done(struct dlist_iterable *it);



#ifdef __cplusplus
}
#endif

#endif