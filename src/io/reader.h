#ifndef H_READER_IO_API
#define H_READER_IO_API

#include "nd.h"
#include <setjmp.h>

#ifdef __cplusplus
extern "C" {
#endif


    /// This interface abstracts over readers that the CachedReader can use to
    /// load data.
    struct Reader {
        void        (*Close)(struct Reader *r);
        struct nd   (*GetShape)(struct Reader *r);      ///< returns the shape of the full volume
        struct nd   (*GetChunkShape)(struct Reader *r); ///< returns the shape of the unit volume used for subvolume access.
        size_t      (*GetSubvolumeSizeBytes)(struct Reader *r,struct nd *shape);
        void*       (*GetSubvolume)(struct Reader *r,int64_t *offset,struct nd *shape,unsigned char *buf,size_t bytesof_buf);
        const char* (*GetLastError)(struct Reader *r);
    };

//
// SOME IMPLEMENTED READER APIS
//

struct Reader *Reader_Open_Tiff(const char* filename);
struct Reader *Reader_Open_KLB(const char* filename);

//
// Convenience functions for reflecting the API
//

#define Reader_Close(self)                                     ((self)->Close(self))
#define Reader_GetLastError(self)                              ((self)->GetLastError(self))
#define Reader_GetShape(self)                                  ((self)->GetShape(self))
#define Reader_GetChunkShape(self)                             ((self)->GetChunkShape(self))
#define Reader_GetSubvolumeSizeBytes(self,shape)               ((self)->GetSubvolumeSizeBytes((self),(shape)))
#define Reader_GetSubvolume(self,offset,shape,buf,bytesof_buf) ((self)->GetSubvolume((self),(offset),(shape),(buf),(bytesof_buf)))


#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* header guard */

/* How should a Reader implementation work?

Opening and Closing
-------------------

The Reader is "Opened" by whatever creates the struct Reader object.
A caller should call the Close() method to notify the implementation
that it is done with the object.  The implementation may use this 
method finalize any associated resources.

Error Handling
--------------

GetLastError should always return NULL unless some operation on the 
Reader encountered an error.  Then it should return a NULL-terminated
string containing an informative error message.

The rest of the API doesn't really indicate success or failure.  So 
GetLastError() has to be called to check for an error state.

Implementations should assume that GetLastErorr() may be checked 
infrequently.  Internally, the implementation should be able to 
handle an unchecked error state.  For example, calls could detect
the error state and then trivially return.

Reading a full volume
---------------------

Should follow the pattern:

    struct Reader *r=CreateMyReader(args);
    struct nd shape=Reader_GetShape(r);
    ... 
        make sure enough memory is available in whatever
        is pointed to by buf 
    ...
    Reader_GetSubvolume(r,0,&shape,buf,bytesof_buf);
    Reader_Close(r);

Note: A NULL passed for the offset array should default to an offset of 0.

Reading a subvolume
-------------------

 - offset and shape form a request bounding box we'd like to fill
 - It should be expected that the request bounding box has to be
   modified to account for chunk size.

        - Shape might get expanded to the nearest chunk.
        - Offset might get shifted to the nearest chunk origin outside the
            request volume.

        In both cases, the request volume does not shrink.

        - The request shape should be clipped to the full volume.

        This is the one case where the request volume might shring.

        GetSubvolumeSizeBytes() doesn't necessarily know if something will clip,
        so it should act as if the offset is 0.  The intention is to return
        an upper bound on the amnount of memory required to fill a request.
        The request shape might still clip against the full volume.

- Subvolume read should iterate over chunks to properly fill the request
    - shape and offset might get adjust for chunk alignment
    - buf (and bytesof_buf) must be large enough to accomidate the
      potentially expanded aligned shape.

*/
