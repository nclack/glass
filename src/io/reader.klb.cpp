#include <klb_imageIO.h>
#include "reader.h"
#include <exception>
#include <stdarg.h>
#include <windows.h>

struct klb_reader;

static void logger(klb_reader *ctx,int is_err, const char* file,int line,const char* function,const char* fmt,...);

#define LOG(...) logger(self,0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(self,1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e,...) do{if(!(e)) {ERR(__VA_ARGS__);goto Error;}}while(0)

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

static const struct messages {
    const char
        *exception,
        *uncaught_exception,
        *failed_to_read_header
        ;
} messages={
    "KLB Error: Exception -\n\t%s",
    "KLB Error: Uncaught exception.",
    "KLB Error: Failed to reader header. Error code %d.\n\t%s",
};

static enum nd_type pxtype_klb_to_nd(enum KLB_DATA_TYPE klb_type_id) {
    switch(klb_type_id) {
        case UINT8_TYPE:return nd_u8;
        case UINT16_TYPE:return nd_u16;
        case UINT32_TYPE:return nd_u32;
        case UINT64_TYPE:return nd_u64;
        case INT8_TYPE:return nd_i8;
        case INT16_TYPE:return nd_i16;
        case INT32_TYPE:return nd_i32;
        case INT64_TYPE:return nd_i64;
        case FLOAT32_TYPE:return nd_f32;
        case FLOAT64_TYPE:return nd_f64;
        default:
            throw std::runtime_error("Invalid KLB pixel type id.");
    }
}

static void restride(struct nd *s) {
    s->strides[0]=1;
    for(unsigned i=0; i < s->ndim; ++i)
        s->strides[i+1]=s->dims[i]*s->strides[i];
}

static size_t nbytes(const struct nd * shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape->strides[shape->ndim]*bpp[shape->type];
}

static unsigned ndim(uint32_t *xyzct) {
    unsigned n=5;
    while(n-->0 && xyzct[n]==1);
    return n+1;
}

static nd make_shape(std::uint32_t *shape,KLB_DATA_TYPE klb_type_id) {
    nd s;
    s.ndim=ndim(shape);
    s.type=pxtype_klb_to_nd(klb_type_id);
    for(int i=0;i<KLB_DATA_DIMS;++i) 
        s.dims[i]=shape[i];
    restride(&s);
    return s;
}

struct klb_reader : public Reader {
    klb_imageIO ctx;
    struct nd shape,block;
    const char* log;

    klb_reader(const char* filename) : ctx(filename), log(0) {
        int ecode;
        if(ecode=ctx.readHeader()) {
            klb_reader *self=this;
            ERR(messages.failed_to_read_header,ecode,filename);
            return;
        }
        // memoize header and chunk info
        shape=make_shape(ctx.header.xyzct,ctx.header.dataType);
        // ctx.header.blockSize[0]*=4;
        // ctx.header.blockSize[1]*=4;
        // ctx.header.blockSize[2]*=4;
        block=make_shape(ctx.header.blockSize,ctx.header.dataType);
    }
};

static void Close(struct Reader *r) {
    klb_reader *self=static_cast<klb_reader*>(r);
    try {
        delete self;
    } catch(std::exception &e) {
        ERR(messages.exception,e.what());
    } catch(...) {
        ERR(messages.uncaught_exception);
    }
}

/// returns the shape of the full volume
static struct nd GetShape(struct Reader *r) {
    klb_reader *self=static_cast<klb_reader*>(r);
    return self->shape;
}

/// returns the shape of the unit volume used for subvolume access.
static struct nd GetChunkShape(struct Reader *r) {
    klb_reader *self=static_cast<klb_reader*>(r);
    return self->block;
} 

static size_t GetSubvolumeSizeBytes(struct Reader *r,struct nd *shape) {
    klb_reader *self=static_cast<klb_reader*>(r);
    return nbytes(shape);
}

static void* GetSubvolume(struct Reader *r,int64_t *offset,struct nd *shape,unsigned char *buf,size_t bytesof_buf) {
    klb_reader *self=static_cast<klb_reader*>(r);
    try {
        klb_ROI roi;
        for(int i=0;i<KLB_DATA_DIMS;++i) {
            roi.xyzctLB[i]=(uint32_t)offset[i];
            roi.xyzctUB[i]=(uint32_t)(offset[i]+shape->dims[i]-1);
        }
        memset(buf,0,bytesof_buf);
        self->ctx.readImage((char*)buf,&roi,-1);
        return buf;
    } catch(std::exception &e) {
        ERR(messages.exception,e.what());
    } catch(...) {
        ERR(messages.uncaught_exception);
    }
    return 0;
}

static const char* GetLastError(struct Reader *r) {
    klb_reader *self=static_cast<klb_reader*>(r);
    try {
        return self->log;
    } catch(std::exception &e) {
        ERR(messages.exception,e.what());
    } catch(...) {
        ERR(messages.uncaught_exception);
    }
}

//
struct Reader *Reader_Open_KLB(const char* filename) {
    klb_reader *self=0;
    try {
        self=new klb_reader(filename);
        self->Close=Close;
        self->GetShape=GetShape;
        self->GetChunkShape=GetChunkShape;
        self->GetSubvolumeSizeBytes=GetSubvolumeSizeBytes;
        self->GetSubvolume=GetSubvolume;
        self->GetLastError=GetLastError;
        return self;
    } catch(std::exception &e) {
        ERR(messages.exception,e.what());
    } catch(...) {
        ERR(messages.uncaught_exception);
    }
    return self;
}

static void logger(klb_reader *ctx,int is_err,const char* file,int line,const char* function,const char* fmt,...) {
    static char buf[1024]={0};
    int c=0;
    va_list ap;
    c=snprintf(buf,sizeof(buf),"%s(%d): %s()\n\t",file,line,function);
    va_start(ap,fmt);
    c+=vsnprintf(buf+c,sizeof(buf)-c,fmt,ap);
    va_end(ap);
    buf[c]='\n';
    OutputDebugStringA(buf);
    if(ctx && is_err)
        ctx->log=buf;
}