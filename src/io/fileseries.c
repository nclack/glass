#include "fileseries.h"
#include <windows.h> // for debug output and for mutex
#include <stdlib.h>
#include <stdio.h>
#include <lualib.h>
#include <lauxlib.h>
#include "tiff.reader.api.h"

#define countof(e) (sizeof(e)/sizeof(*(e)))

static void logger(FileSeriesReader *ctx,int is_err, const char* file,int line,const char* function,const char* fmt,...);

#define LOG(...) logger(self,0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) logger(self,1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e,...) do{if(!(e)) {ERR(__VA_ARGS__);goto Error;}}while(0)


static const struct messages {
    const char
        *allocation_failure,
        *loadscript_failure,
        *runscript_failure,
        *not_table,
        *not_string,
        *not_int,
        *empty_table,
        *stack_read_error,
        *filename_no_extension,
        *filename_ends_with_period,
        *filename_extension_not_recognized,
        *subvolume_buffer_not_large_enough,
        *offset_out_of_bounds
        ;
} messages={
    "FileSeries Error:  Could not allocate %llu bytes.",
    "FileSeries Error:  Could not open fileseries configuration file\n\t%s",
    "FileSeries Error:  Could not interpret fileseries configuration file: %s",
    "FileSeries Error:  Did not find returned table.\n\t%s",
    "FileSeries Error:  %s must be a string.",
    "FileSeries Error:  %s must be an integer.",
    "FileSeries Error:  Received an empty table.",
    "FileSeries Error:  Could not read from a file specified by the fileseries.\n\t%s\n\t%s",
    "FileSeries Error:  Couldn't find a filename extension.\n\t%s",
    "FileSeries Error:  Filename ended with a '.'.\n\t%s",
    "FileSeries Error:  Filename extension not recognized.\n\t%s",
    "FileSeries Error:  Buffer size provided for subvolume read was not large enough for aligned volume.",
    "FileSeries Error:  Request offset is outside of the volume.",
};

struct ctx {
    lua_State *L;
    int mn,mx;       ///< min/max indexes into the fileseries table
    struct nd shape; ///< shape of the full fileseries
    struct nd unit;  ///< chunk size used for subvolume access in referenced files
    int ndim_stack;  ///< number of dimensions in the files
    SRWLOCK lock;    ///< protects access to lua state
};

static void logger(FileSeriesReader *ctx,int is_err,const char* file,int line,const char* function,const char* fmt,...) {
    static char buf[1024]={0};
    int c=0;
    va_list ap;
    c=snprintf(buf,sizeof(buf),"%s(%d): %s()\n\t",file,line,function);
    va_start(ap,fmt);
    c+=vsnprintf(buf+c,sizeof(buf)-c,fmt,ap);
    va_end(ap);
    buf[c]='\n';
    OutputDebugStringA(buf);
    if(is_err)
        ctx->log=buf;
}

static void restride(struct nd *s) {
    s->strides[0]=1;
    for(unsigned i=0; i < s->ndim; ++i)
        s->strides[i+1]=s->dims[i]*s->strides[i];
}

static const struct file_extension_association {
    const char* extension;
    struct Reader* (*open)(const char* filename);
} file_extension_assocations[]= {
    {"tif" ,Reader_Open_Tiff},
    {"klb" ,Reader_Open_KLB},
    {"lua" ,Reader_Open_FileSeries},
    {"tiff",Reader_Open_Tiff},
    {0,0}
};

static struct Reader* detect_and_open_reader(FileSeriesReader *self,const char *filename) {
    const char* c=filename+strlen(filename);
    while(*c!='.' && c>=filename)
        --c;
    if(c==filename) {
        ERR(messages.filename_no_extension,filename);
        return 0;
    }
    ++c; // move past the dot
    if(!strlen(c)) {
        ERR(messages.filename_ends_with_period,filename);
        return 0;
    }
    
    for(int i=0;file_extension_assocations[i].extension;++i) {
        if(strcmp(c,file_extension_assocations[i].extension)==0)
            return file_extension_assocations[i].open(filename);
    }
    ERR(messages.filename_extension_not_recognized,filename);
    return 0;
}

FileSeriesReader FileSeriesReader_Open(const char* filename, int nargs, const char **args) {
    FileSeriesReader R={0},*self=&R;
    struct ctx *ctx;
    
    CHECK(self->handle=malloc(sizeof(struct ctx)),
          messages.allocation_failure,sizeof(struct ctx));
    ctx=self->handle;
    InitializeSRWLock(&ctx->lock);

    lua_State *L=luaL_newstate();
    luaL_openlibs(L);

    CHECK(luaL_loadfile(L,filename)==0, messages.loadscript_failure,lua_tostring(L,-1));
    for(int i=0;i<nargs;++i)
        lua_pushstring(L,args[i]);
    CHECK(lua_pcall(L,nargs,LUA_MULTRET,0)==0, messages.runscript_failure,lua_tostring(L,-1));
    CHECK(lua_istable(L,-1), messages.not_table,lua_tostring(L,-1));

    int isnum;
    lua_pushnil(L);
    // first iteration, populate mn and mx
    if(lua_next(L,-2)!=0) {
        ctx->mn=ctx->mx=(int)lua_tointegerx(L,-2,&isnum);
        CHECK(isnum,messages.not_int,"key");
        CHECK(lua_type(L,-1)==LUA_TSTRING,messages.not_string,"Table value");
        lua_pop(L,1);
    } else {
        ERR(messages.empty_table);
        goto Error;
    }
    // loop over key,value pairs
    while(lua_next(L,-2)!=0) {
        int v=(int)lua_tointegerx(L,-2,&isnum);
        CHECK(isnum,messages.not_int,"key");
        CHECK(lua_type(L,-1)==LUA_TSTRING,messages.not_string,"Table value");
        ctx->mn=min(ctx->mn,v);
        ctx->mx=max(ctx->mx,v);
        lua_pop(L,1);
    }

    // get first file to define shape
    lua_pushnil(L);
    lua_next(L,-2);
    const char *t=lua_tostring(L,-1);

    if(self->log)
        goto Error;

    {
        struct Reader *R=detect_and_open_reader(self,t);
        if(self->log=Reader_GetLastError(R))
            goto Error;
        ctx->shape=Reader_GetShape(R);
        ctx->unit=Reader_GetChunkShape(R);
        Reader_Close(R);
    }

    lua_pop(L,2);

    // add file series dimensions to shape
    ctx->ndim_stack=ctx->shape.ndim;
    ctx->shape.dims[ctx->shape.ndim++]=ctx->mx-ctx->mn+1;
    restride(&ctx->shape);
    // add file series dimensions to unit
    ctx->unit.dims[ctx->unit.ndim++]=1;
    restride(&ctx->unit);

    // assert table is on top of the stack
    CHECK(lua_istable(L,-1),messages.not_table,"Top of stack must be a table.");
    ctx->L=L;
    return *self;
Error:
    lua_close(L);
    free(self->handle);
    self->handle=0;
    return *self;
}

void FileSeriesReader_Close(FileSeriesReader *r) {
    if(r->handle) {
        struct ctx *ctx=(struct ctx*)r->handle;
        lua_close(ctx->L);
    }
    free(r->handle);
    r->handle=0;
}

struct nd FileSeriesReader_GetShape(FileSeriesReader *r) {
    if(r->log) return (struct nd) { 0 };
    struct ctx *ctx=(struct ctx*)r->handle;
    return ctx->shape;
}

static size_t nbytes(const struct nd * shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape->strides[shape->ndim]*bpp[shape->type];
}

static void clip(const uint64_t *bounds, int64_t *offset,uint64_t *request, int ndim) {
    if(offset) {
        for(int i=0;i<ndim;++i) {
            offset[i] =min(offset[i],bounds[i]-1);
            offset[i] =max(offset[i],0);
            request[i]=min(bounds[i]-offset[i],request[i]);
        }
    } else {
        for(int i=0;i<ndim;++i)
            request[i]=min(bounds[i],request[i]);
    }
}

static void align(const uint64_t *unit, int64_t *offset,uint64_t *request, int ndim) {
    // align offset
    if(offset) {
        for(int i=0;i<ndim && unit[i];++i) {
            const int64_t o=(offset[i]/unit[i])*unit[i], // floor
                        d=offset[i]-o;
            offset[i]=o;
            request[i]+=d; // expand by delta
        }
    }
    // align bounds
    for(int i=0;i<ndim && unit[i];++i) {
        const uint64_t a=request[i],b=unit[i];
        request[i]=((a+b-1)/b)*b; // ceil
    }
}

size_t FileSeriesReader_GetSubvolumeSizeBytes(FileSeriesReader *r,struct nd *shape) {
    struct ctx *ctx=(struct ctx*)r->handle;
    if(r->log) return 0;
    // override pixel type
    shape->type=ctx->shape.type;
    clip(ctx->shape.dims,0,shape->dims,countof(shape->dims));
    align(ctx->unit.dims,0,shape->dims,countof(shape->dims));
    restride(shape);
    return nbytes(shape);
}

static int is_offset_in_bounds(const int64_t *o, const uint64_t *d,int ndim) {
    for(int i=0;i<ndim;++i) {
        if(o[i]<0) return 0;
        if(d[i]>0 && o[i]>=d[i]) return 0;
    }
    return 1;
}

int FileSeriesReader_GetSubvolume(
    FileSeriesReader *self,
    int64_t *offset,
    struct nd *shape,
    unsigned char *buf,
    size_t bytes_of_buf
) {
    struct ctx *ctx=(struct ctx*)self->handle;
    if(self->log) return 0;
    CHECK(is_offset_in_bounds(offset,ctx->shape.dims,countof(ctx->shape.dims)),
        messages.offset_out_of_bounds);
    // adjust request shape
    shape->type=ctx->shape.type; // override pixel type
    clip(ctx->shape.dims,offset,shape->dims,countof(shape->dims));
    align(ctx->unit.dims,offset,shape->dims,countof(shape->dims));
    restride(shape);
    CHECK(nbytes(shape)<=bytes_of_buf,messages.subvolume_buffer_not_large_enough);

    // FIXME: Only does time at the moment
    // TODO: extend to higher dimensional keys. e.g. (time,view,etc)
    int idim=ctx->ndim_stack;
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    size_t stride_bytes=shape->strides[idim]*bpp[shape->type];
    AcquireSRWLockExclusive(&ctx->lock);
    for(int i=0;i<shape->dims[idim];++i) {
        lua_pushinteger(ctx->L,i+ctx->mn+offset[idim]);
        lua_gettable(ctx->L,-2); //pops key, pushes value
        
        const char *t=lua_tostring(ctx->L,-1);
        {
            struct Reader *R=detect_and_open_reader(self,t);
            lua_pop(ctx->L,1); // remove value, now table is at top of stack
            if(self->log) {
                ERR(messages.stack_read_error,self->log,t);
                goto Error;
            }
            CHECK(R->GetSubvolume(R,offset,shape,buf+i*stride_bytes,bytes_of_buf),
                messages.stack_read_error,self->log,t);
            Reader_Close(R);
        }
    }
    ReleaseSRWLockExclusive(&ctx->lock);

    return 1;
Error:
    return 0;
}

//
// Reader API
//

#include <stddef.h>
#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct fileseries_reader {
    struct Reader R;
    FileSeriesReader F;
};

static void Close(struct Reader *r) {
    struct fileseries_reader *self=containerof(r,struct fileseries_reader,R);
    FileSeriesReader_Close(&self->F);
    free(self);
}

static struct nd GetShape(struct Reader *r) {
    struct fileseries_reader *self=containerof(r,struct fileseries_reader,R);
    return FileSeriesReader_GetShape(&self->F);
}

static struct nd GetChunkShape(struct Reader *r) {
    struct fileseries_reader *self=containerof(r,struct fileseries_reader,R);
    if(!self->F.handle)
        return (struct nd) { 0 };
    struct ctx *ctx=(struct ctx*)self->F.handle;
    if(!self->F.handle)
        return (struct nd) { 0 };

    return ctx->unit;

    // shape=ctx->shape;

    // // TiffReader doesn't have sub-volume access
    // // So unit volume is the size of the tiff stack.
    // // The remaining dimensions are random-access, so set those to 1.
    // for(unsigned i=ctx->ndim_stack;i<ctx->shape.ndim;++i)
    //     shape.dims[i]=1;
    // restride(&shape);

    // return shape;
}

static size_t GetSubvolumeSizeBytes(struct Reader *r,struct nd *shape) {
    struct fileseries_reader *self=containerof(r,struct fileseries_reader,R);
    return FileSeriesReader_GetSubvolumeSizeBytes(&self->F,shape);
}

static void* GetSubvolume(struct Reader *r,int64_t *offset,struct nd *shape,unsigned char *buf,size_t bytesof_buf) {
    struct fileseries_reader *self=containerof(r,struct fileseries_reader,R);
    if(FileSeriesReader_GetSubvolume(&self->F,offset,shape,buf,bytesof_buf))
        return buf;
    return 0;
}

static const char* FSGetLastError(struct Reader *r) {
    struct fileseries_reader *self=containerof(r,struct fileseries_reader,R);
    return self->F.log;
}

struct Reader * Reader_Open_FileSeries_Args(
    const char* filename,
    int nargs,
    const char **args) 
{
    struct fileseries_reader *self=malloc(sizeof(*self));
    *self=(struct fileseries_reader) {
        .R=(struct Reader) {
            .Close=Close,
            .GetShape=GetShape,
            .GetChunkShape=GetChunkShape,
            .GetSubvolumeSizeBytes=GetSubvolumeSizeBytes,
            .GetSubvolume=GetSubvolume,
            .GetLastError=FSGetLastError
        },
        .F=FileSeriesReader_Open(filename,nargs,args)
    };
    return &self->R;
}

struct Reader * Reader_Open_FileSeries(const char* filename) {
    return Reader_Open_FileSeries_Args(filename,0,0);
}

/* NOTE
 * 
 * Should always leave the table at the top of the lua stack.
 * 
 * TODO: detect discontinuous keys
 * - Maybe should just skip non-integer keys
 * - Maybe support coroutines as generators of file-series? 
 * 
 * One way of generalizing this to higher dims is to thing 
 * more along the lines of a stitching problem:  Each file
 * has a bounding box and a transform.  The self resolves
 * which bounding box gets hit and loads.  That's not so 
 * easy for people to use though.
 * 
 * 
 * FIXME: protect multithreaded access to lua state
 *  - Could support 1 lua state per thread
 *  - otherwise probably need a mutex (CURRENT APPROACH)
 *  - or, could make sure lua state is short lived by
 *    reading everything out that we need
 *    
 *    Probably the best this is to read everything into a
 *    hashmap and then go from there.  No mutex required.
 *    OTOH, the hashmap needs dynamic allocation when
 *    the fileseries is opened.
 *    
 *    This also means any logical errors in the lua happen
 *    on open.
 */