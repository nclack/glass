#ifndef H_CACHED_IO_API
#define H_CACHED_IO_API

#include "reader.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct CachedReader {
        void *handle;        /**< A pointer to the (abstract) file context */
        const char *log;     /**< Points to a buffer with error information if something goes wrong. */
    };
    typedef struct CachedReader CachedReader;

    /// \param reader             A reference to the reader that will be used to
    ///                           actually resolve requests and load data.
    /// \param item_shape         Each requested chunk must be the same shape.
    ///                           This determines the size that will be reserved per
    ///                           element.  The total number of cached elements
    ///                           will be floor(nbytes/(element_size_bytes+overhead))
    /// \param buffer             The cached will be stored in this buffer
    ///                           allocated by the caller.
    /// \param nbytes             The size of the buffer in bytes.
    CachedReader CachedReader_Open    (struct Reader *reader, void *buffer, size_t nbytes);
    void         CachedReader_Close   (CachedReader *r);

    /// \returns a description of the full shape of the array.
    struct nd    CachedReader_GetShape(CachedReader *r);
    /// \returns the shape of a unit subvolume
    struct nd    CachedReader_GetUnitShape(CachedReader *r);

    // The requested offset might not be what is returned.
    // The offset parameter is modified to reflect the origin of the output volume
    // The returned pointer points to the requested voxel.  This will not necessarily 
    // correspond to the modified offset.

    enum CachedReaderStatus {
        CachedReaderStatus_Error,
        CachedReaderStatus_Miss,
        CachedReaderStatus_Loading,
        CachedReaderStatus_Done,
    };

    size_t       CachedReader_GetSubvolumeSizeBytes(CachedReader *r);
    struct nd    CachedReader_GetSubvolume(CachedReader *r,int64_t *offset,unsigned char **buf,enum CachedReaderStatus *status);
    void         CachedReader_PreCache(CachedReader *r,int64_t *offset);

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* header guard */
