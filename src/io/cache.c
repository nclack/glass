#include "cache.h"
#include "dlist.h"
#include <windows.h> // for threading 

#define LOG(...) logger(__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)

#include <stdarg.h>
#include <stdio.h>

static void logger(const char* file,int line,const char* function,const char* fmt,...) {
    char buf[1024]={0};
    int c=0;
    va_list ap;
    //c=snprintf(buf,sizeof(buf),"%s(%d): %s()\n\t",file,line,function);
    va_start(ap,fmt);
    c+=vsnprintf(buf+c,sizeof(buf)-c,fmt,ap);
    va_end(ap);
    buf[c]='\n';
    OutputDebugStringA(buf);
}

static uint64_t RequestCount=0;

struct cache_item {
    uint64_t key;
    uint64_t timestamp;
    uint8_t flags;
    int64_t offset[10];
    uint8_t *data;
};

struct work {
    struct work *next;
    void(*job)(struct request*);
};

/// LIFO work queue implemented as a singly-linked list.
struct lifo {
    int running;
    int count;
    HANDLE thread;
    SRWLOCK lock;
    struct work *head;
}; 

struct cache_ctx {
    struct Reader *r;      ///< the source reader that is used to resolve requests
    size_t capacity,       ///< size of total cache buffer in bytes
           elem_bytes;     ///< element size in bytes
    struct nd unit;        ///< the unit shape of a cached item

    struct lifo queue;     ///< a thread pool and queue for asynchronously resolving reqeusts

    uint8_t *data;         ///< buffer used to hold loaded volumes
    size_t table_capacity; ///< # elements in the table
    size_t nused;          ///< number of slots in the table that are used.
    size_t log2_cap;       ///< log2 of # elements that can fit in the table
    struct cache_item entries[]; ///< a hash table that indexes requests, mapping to slots in the buffer
};

enum {
    ITEM_LOCK     = 1, ///< the item slot is in use by a loading thread
    ITEM_ERROR    = 2, ///< something went wrong when loading data. item->data may hold a NULL-terminated string describing the error.
    ITEM_ACQUIRED = 4, ///< the item slot is in use by the cache (contains data)
};

#define countof(e) (sizeof(e)/sizeof(*(e)))
#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct request {
    //struct dlist_item item;
    struct work work;
    CachedReader *r;
    int64_t offset[10];
    uint64_t timestamp;
};

/// thread procedure to work through the queue
static void dowork(void *param) {
    struct lifo *queue=(struct lifo*)param;
    while(queue->running) {
        while(queue->running && queue->head) {
            struct work *w=0;
            // Pop
            AcquireSRWLockExclusive(&queue->lock);
            if(queue->head) { // double check now that the lock is acquired
                w=queue->head;
                queue->head=w->next;
                queue->count--;
            }
            ReleaseSRWLockExclusive(&queue->lock);

            // Execute
            if(w) {
                struct request *req=containerof(w,struct request,work);
                w->job(req);
                free(req); // cleanup
            }
        }
        // wait a bit so we don't needlessly burn the cpu.  
        // also want requests to stack up a bit
        Sleep(10); 
    }
}

static void defer(struct cache_ctx *ctx,struct work* w) {
    struct lifo *q=&ctx->queue;
    // push onto LIFO work queue
    AcquireSRWLockExclusive(&q->lock);
    w->next=q->head;
    q->head=w;
    q->count++;
    ReleaseSRWLockExclusive(&q->lock);
}

static void restride(struct nd *s) {
    s->strides[0]=1;
    for(unsigned i=0; i < s->ndim; ++i)
        s->strides[i+1]=s->dims[i]*s->strides[i];
}

static size_t nbytes(const struct nd * shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape->strides[shape->ndim]*bpp[shape->type];
}

static void align_offset(const struct nd *unit, int64_t *offset, int64_t *delta) {
    for(unsigned i=0;i<unit->ndim;++i){
        int64_t v=(offset[i]/unit->dims[i])*unit->dims[i];
        delta[i]=offset[i]-v;
        offset[i]=v;
    }
}

static uint64_t offset_hash(const int64_t *offset, int ndim) {
    int siphash(uint8_t *out,const uint8_t *in,uint64_t inlen,const uint8_t *k);
    const static char salt[16]="sadfG#$5#pu^9asm";
    uint64_t out;
    siphash((unsigned char*)&out,(unsigned char*)offset,ndim*sizeof(*offset),salt);
    return out;
}

#define NPROBE (8)
#define PROBE_FUNC(p) (p)

/*
    Hash table lookup to find and lock space for loading an item at offset.
    If no slot can be found, the cache is full and an item must be evicted.
    Always returns a pointer to an item in the cache; never NULL.

    FIXME: only evict if timestamp for this request is newer
           than the timestamp for the requested item
           Avoid evicting newer items when uncovering old requests
*/
static struct cache_item *acquire(struct cache_ctx* ctx,const int64_t *offset,uint64_t timestamp) {
    const uint64_t mask=ctx->table_capacity-1;
    uint64_t hash=offset_hash(offset,ctx->unit.ndim);
    int64_t argmax=-1;
    int64_t  mx=0;

    for(uint64_t probe=0;probe<NPROBE;++probe) {
        for(uint64_t k=hash;k;k>>=ctx->log2_cap) {
            uint64_t pos=(k+PROBE_FUNC(probe))&mask;
            if(ctx->entries[pos].flags&ITEM_ACQUIRED) {
                if(ctx->entries[pos].key==hash)
                    return 0; // already acquired

                if(ctx->entries[pos].timestamp<timestamp) {
                    // Consider pos for eviction:
                    // compute distance between hit item and request
                    // the max distance item will be chosen for eviction
                    int64_t r2=0,
                        *o=ctx->entries[pos].offset;
                    for(unsigned i=0;i<ctx->unit.ndim;++i) {
                        int64_t delta=offset[i]-o[i];
                        r2+=delta*delta;
                    }
                    if(mx<r2) {
                        argmax=pos;
                        mx=r2;
                    }
                }

            } else {
                struct cache_item *it=ctx->entries+pos;
                memcpy(it->offset,offset,sizeof(it->offset));
                it->key=hash;
                it->timestamp=timestamp;
                it->flags|=ITEM_ACQUIRED;
                ctx->nused++;
                return it;
            }
        }
    }

    // Couldn't insert must evict
    // Will choose the item farthest away (spatially) from this request.

    //  - How to track/monitor performance?  Is it important?
    //  - Possibly retain lower resolution versions of evicted blocks in
    //    a hierarchical caching scheme.

    if(argmax>=0) {
        struct cache_item *it=ctx->entries+argmax;
        memcpy(it->offset,offset,sizeof(it->offset));
        it->key=hash;
        it->timestamp=timestamp;
        it->flags|=ITEM_ACQUIRED;
        memset(it->data,0,nbytes(&ctx->unit));
        return it;
    }

    // Getting here means there was no older item 
    // to evict. Should ignore.
    return 0;
}

static struct cache_item *lookup(struct cache_ctx* ctx, const int64_t *offset) {
    const uint64_t mask=ctx->table_capacity-1;
    uint64_t hash=offset_hash(offset,ctx->unit.ndim);
    for(uint64_t probe=0;probe<NPROBE;++probe) {
        for(uint64_t k=hash;k;k>>=ctx->log2_cap) {
            uint64_t pos=(k+PROBE_FUNC(probe))&mask;
            if( (ctx->entries[pos].flags&ITEM_ACQUIRED)
                &&ctx->entries[pos].key==hash) {
                return ctx->entries+pos;
            }
        }
    }
    return 0;
}


static void load(struct request *request) {
    struct cache_ctx *ctx=(struct cache_ctx*)request->r->handle;
    struct cache_item *item=acquire(ctx,request->offset,request->timestamp);
    if(!item || (item->flags&ITEM_LOCK)==ITEM_LOCK)
        return;
    item->flags|=ITEM_LOCK;
    struct nd shape=ctx->unit;
    size_t bytesof_data=nbytes(&shape);
    LOG("[CACHE] Loading: [%3d,%3d,%3d,%3d] %d",
        (int)request->offset[0],
        (int)request->offset[1],
        (int)request->offset[2],
        (int)request->offset[3],
        ctx->queue.count);
    if(!ctx->r->GetSubvolume(ctx->r,request->offset,&shape,item->data,bytesof_data)) {
        item->flags|=ITEM_ERROR;
        strcpy_s(item->data,bytesof_data,ctx->r->GetLastError(ctx->r));
    }
    item->flags=item->flags&~ITEM_LOCK;
}

static int is_offset_equal(const int64_t *a,const int64_t *b) {
    for(int i=0;i<10;++i)
        if(a[i]!=b[i])
            return 0;
    return 1;
}

static int is_already_top_of_queue(const struct cache_ctx *ctx,const int64_t *offset) {
    if(!ctx->queue.head)
        return 0;
    struct request *req=containerof(ctx->queue.head,struct request,work);
    return is_offset_equal(req->offset,offset);
}


static int is_anywhere_in_queue(struct cache_ctx *ctx,const int64_t *offset) {
    int any=0;
    AcquireSRWLockExclusive(&ctx->queue.lock);
    for(struct work* cur=ctx->queue.head;!any && cur->next;cur=cur->next) {
        struct request *req=containerof(cur,struct request,work);
        any|=is_offset_equal(req->offset,offset);
    }
    ReleaseSRWLockExclusive(&ctx->queue.lock);
    return any;
}

static void request(CachedReader *r, int64_t *offset) {
    struct cache_ctx *ctx=(struct cache_ctx*)r->handle;
    // 1. check if request is already on queue
    //
    //    flooding the queue with the same requests can have
    //    weird effects.  Since this is LIFO, an new request
    //    might evict an item that still has corresponding
    //    requests on the queue (if requests get spammed).
    //    When that old requests gets uncovered, it will 
    //    evict the newer guy.
    //
    //    This also saves on malloc/frees
    if(is_already_top_of_queue(ctx,offset))
        return;
    if(ctx->queue.count>1000)
        if(is_anywhere_in_queue(ctx,offset))
            return;

    // 2. make and post the request
    struct request *req=malloc(sizeof(struct request));
    *req=(struct request){
        .work.next=0,
        .work.job=load,
        .r=r,
        .timestamp=RequestCount++
    };
    for(unsigned i=0;i<ctx->unit.ndim;++i)
        req->offset[i]=offset[i];
#if 0
    LOG("[CACHE] Request: [%3d,%3d,%3d,%3d]",
        (int)req->offset[0],
        (int)req->offset[1],
        (int)req->offset[2],
        (int)req->offset[3]);
#endif        
    defer(ctx,&req->work);
}

static void request_low_priority(CachedReader *r, int64_t *offset) {
    struct cache_ctx *ctx=(struct cache_ctx*)r->handle;
    // if the queue is empty, this is a normal request
    if(!ctx->queue.head) {
        request(r,offset);
        return;
    }
    struct lifo *q=&ctx->queue;
    // push onto LIFO work queue
    AcquireSRWLockExclusive(&q->lock);

    // 1. find end of queue
    struct work *cur,*end=0;
    for(cur=ctx->queue.head;cur->next;cur=cur->next);
    end=cur;

    // 2. don't duplicate that request
    struct request *req=containerof(end,struct request,work);
    if(is_offset_equal(req->offset,offset))
        goto Finalize;

    // 3. make and post the request
    req=malloc(sizeof(struct request));
    *req=(struct request){
        .work.next=0,
        .work.job=load,
        .r=r,
        .timestamp=RequestCount++
    };
    for(unsigned i=0;i<ctx->unit.ndim;++i)
        req->offset[i]=offset[i];
    
    struct work *w=&req->work;
    end->next=w;
    q->count++;
Finalize:
    ReleaseSRWLockExclusive(&q->lock);
}

static uint8_t* get(CachedReader *r,int64_t *offset,int *is_loading) {
    struct cache_ctx *ctx=(struct cache_ctx*)r->handle;
    struct cache_item *item=lookup(ctx,offset);
    if(!item) return 0;
    if( (item->flags&ITEM_ERROR)==ITEM_ERROR) {
        r->log=item->data;
        return 0;
    }
    if(is_loading) *is_loading=(item->flags&ITEM_LOCK)==ITEM_LOCK;
    return item->data;
}

// for e in (2^(x-1),2^x] return x
static uint64_t nextpow2(uint64_t v) {
    v--;
    v|=v>>1;
    v|=v>>2;
    v|=v>>4;
    v|=v>>8;
    v|=v>>16;
    v|=v>>32;
    v++;
    return v;
}

// for e in [2^(x-1),2^x) return x-1
//
// nextpow2(e+1) = x for e in (2^(x-1)-1,2^x-1]
//                equiv to in [2^(x-1),2^x)
// so
// nextpow2(e+1)>>1 = x-1 for e in [2^(x-1),2^x)
static uint64_t prevpow2(uint64_t v) {
    return nextpow2(v+1)>>1;
}

static uint64_t log2_of_pow2(uint64_t v) {
    static const uint64_t b[]={
        0xAAAAAAAAAAAAAAAAULL, 0xCCCCCCCCCCCCCCCCULL, 0xF0F0F0F0F0F0F0F0ULL,
        0xFF00FF00FF00FF00ULL, 0xFFFF0000FFFF0000ULL, 0xFFFFFFFF00000000ULL};
    register uint64_t r=(v & b[0])!=0;
    r|=((v & b[5])!=0)<<5;
    r|=((v & b[4])!=0)<<4;
    r|=((v & b[3])!=0)<<3;
    r|=((v & b[2])!=0)<<2;
    r|=((v & b[1])!=0)<<1;
    return r;
}

//
// I N T E R F A C E
//


CachedReader CachedReader_Open(struct Reader *reader, void *buffer, size_t bytesof_buf) {    
    struct cache_ctx *ctx=(struct cache_ctx*)buffer;
    if(!buffer)
        return (CachedReader) { .log="Buffer is NULL.  Possible allocation failure." };
    if(bytesof_buf < sizeof(*ctx))
        return (CachedReader) { .log="Buffer is too small to initialize cache." };
    ctx->unit=reader->GetChunkShape(reader);
    const char* err;
    if((err=reader->GetLastError(reader)))
        return (CachedReader) { .log=err };
    *ctx=(struct cache_ctx) {
        .r=reader,
        .capacity=bytesof_buf,
        .elem_bytes=nbytes(&ctx->unit),
        .unit=ctx->unit
    };

    const size_t sizeof_table=bytesof_buf-sizeof(*ctx);
    const size_t sizeof_item=sizeof(struct cache_item)+nbytes(&ctx->unit);
    ctx->table_capacity=prevpow2(sizeof_table/sizeof_item);
    ctx->log2_cap=log2_of_pow2(ctx->table_capacity);
    if(ctx->table_capacity==0)
        return (CachedReader) { .log="Buffer is too small to cache 1 item." };


    const size_t sizeof_index=ctx->table_capacity*sizeof(struct cache_item);
    ctx->data=((uint8_t*)ctx->entries)+sizeof_index;
    for(int i=0;i<ctx->table_capacity;++i) {
        ctx->entries[i]=(struct cache_item){
            .data=ctx->data+i*nbytes(&ctx->unit),
        };
    }

    ctx->queue.running=1;
    InitializeSRWLock(&ctx->queue.lock);
    ctx->queue.thread=CreateThread(0,0,(LPTHREAD_START_ROUTINE)dowork,&ctx->queue,0,0);
    
    return (CachedReader){.handle=ctx};
}

void CachedReader_Close(CachedReader *r) {
    struct cache_ctx* ctx=(struct cache_ctx*)r->handle;
    ctx->queue.running=0;
    WaitForSingleObject(ctx->queue.thread,INFINITE);
    CloseHandle(ctx->queue.thread);
    ctx->r->Close(ctx->r);
}

struct nd CachedReader_GetShape(CachedReader *r) {    
    struct cache_ctx* ctx=(struct cache_ctx*)r->handle;
    return ctx->r->GetShape(ctx->r);
}

struct nd CachedReader_GetUnitShape(CachedReader *r) {
    struct cache_ctx* ctx=(struct cache_ctx*)r->handle;
    return ctx->unit;
}

size_t CachedReader_GetSubvolumeSizeBytes(CachedReader *r) {
    struct cache_ctx *ctx=(struct cache_ctx*)r->handle;
    return nbytes(&ctx->unit);
}

struct nd CachedReader_GetSubvolume(CachedReader *r,int64_t *offset,unsigned char **buf,enum CachedReaderStatus *status) {
    struct cache_ctx *ctx=(struct cache_ctx*)r->handle;
    int64_t delta[countof(ctx->unit.dims)]={0};
    int is_loading=0;
    if(status) *status=CachedReaderStatus_Error;
    if(!r->handle) {
        *buf=0;
        return (struct nd) { 0 };
    }

    align_offset(&ctx->unit,offset,delta);
    if(!(*buf=get(r,offset,&is_loading))) { // checks to see if the request was resolved -- might miss )
        request(r,offset);      // puts the request on a work queue
        if(status) *status=CachedReaderStatus_Miss;
    }
    
    // offset pointer into cached volume
    if(*buf) {
        const int64_t bpp[]={1,2,4,8,1,2,4,8,4,8};
        const int64_t b=bpp[ctx->unit.type];
        for(unsigned i=0;i<ctx->unit.ndim;++i)
            *buf+=b*ctx->unit.strides[i]*delta[i]; // FIXME: should probably also fix output shape if I'm going to do this?  Should probably just avoid offseting...this is weird

        if(status) 
            *status=is_loading
                ? CachedReaderStatus_Loading
                : CachedReaderStatus_Done;
    }
    // LOG("CACHE: %5d/%5d - %f\n",ctx->nused,ctx->table_capacity,ctx->nused/(float)ctx->table_capacity);
    return ctx->unit;
}

void CachedReader_PreCache(CachedReader *r,int64_t *offset) {
    struct cache_ctx *ctx=(struct cache_ctx*)r->handle;
    int64_t delta[countof(ctx->unit.dims)]={0};
    if(!r->handle)
        return;
    // limit the number of outstanding requests to the table size
    if(ctx->queue.count>(ctx->table_capacity-ctx->nused))
        return;
    // once the table is full, no need to precache
    if(ctx->nused>=ctx->table_capacity)
        return;

    align_offset(&ctx->unit,offset,delta); // maps the requested offset to a good chunk offset
    if(!get(r,offset,0)) { // checks to see if the offset is already loaded
        // check to see if the request is already outstanding
        int any=0;
        AcquireSRWLockExclusive(&ctx->queue.lock);
        struct work *cur=ctx->queue.head;
        for(;cur;cur=cur->next) {
            struct request *r=containerof(cur,struct request,work);
            if(is_offset_equal(r->offset,offset)) {
                any=1;
                break;
            }
        }
        ReleaseSRWLockExclusive(&ctx->queue.lock);
        if(any)
            return;
            
        request_low_priority(r,offset);      // puts the request on a work queue
    }
    
    // LOG("PRECACHE: %5d/%5d - %f\n",ctx->nused,ctx->table_capacity,ctx->nused/(float)ctx->table_capacity);
}

/* NOTES

Parameterizing the cache.

  Total data size (eg. 8GB)
  Slot size
  Number of slots?


      Grows
      Eviction triggered by an (internal) allocation failure.


Composition
    Have cache automatically restrict itself to the outer uncached dimension
    CachedReader implements Reader

    A new cached reader could be wrapped around another cached reader to
    cache the last few dimensions.

    Would want to be a little better about avoiding malloc, since the memory
    for the wrapped reader's should come from the cache buffers.

    Would end up modeling one kind of caching behavior based on assumptions
    about outer-vs-inner dimensions.  Wouldn't really extend to octree-style
    stuff for example.  One useful use-case to imagine is that of multiview
    timeseries.  Ordering the view vs time dimension would determine how things
    got evicted.

    Not sure if there's any value in this.  It might simplify the interface
    a bit because you don't have to say anything about the item size.

Exception handling
    Not sure what to do here, but since I'm wrapping other readers it would
    be nice to have something more composable.

    Should at least make the caller provide the allocator so I don't have
    to check the return values all the time.


ITEM_LOCK and ITEM_ACQUIRED might be redundant

Multilevel cache

    Could implement a reader interface for cached reader.
    Progressively halve the buffer handed to each reader and
        store arrays of half dim each.

    On eviction, decimate by 2 and insert into next level. Or compute 
        decimations on load.

    On miss, try to return a downsampled version from the next cache 
    level.

Cancellable requests. Minimize time to serve last requests

    Requests should be handled in a LIFO (last in first out) fashion.

    If a new request comes in while others are being processed, it should
    preempt them.  That might mean canceling/interupting/killing threads.

*/
