#include "reader.h"
#include <tiff.reader.api.h>
#include <stddef.h>
#include <stdlib.h> // malloc

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct tiff_reader {
    struct Reader R;
    struct ScanImageTiffReader T;
};

static void Close(struct Reader *r){
    struct tiff_reader *self=containerof(r,struct tiff_reader,R);
    ScanImageTiffReader_Close(&self->T);
    free(self);
}

/// returns the shape of the full volume
static struct nd GetShape(struct Reader *r){
    struct tiff_reader *self=containerof(r,struct tiff_reader,R);
    return ScanImageTiffReader_GetShape(&self->T);
}

/// returns the shape of the unit volume used for subvolume access.
/// TiffReader doesn't have sub-volume access, so the chunk size
/// is the file size.
static struct nd GetChunkShape(struct Reader *r){
    struct tiff_reader *self=containerof(r,struct tiff_reader,R);
    return ScanImageTiffReader_GetShape(&self->T);
}
    
static size_t GetSubvolumeSizeBytes(struct Reader *r,struct nd *shape){
    struct tiff_reader *self=containerof(r,struct tiff_reader,R);
    return ScanImageTiffReader_GetDataSizeBytes(&self->T);
}

/// TiffReader doesn't have sub-volume access,
/// just returns the whole stack.
static void* GetSubvolume(struct Reader *r,int64_t *offset,struct nd *shape,unsigned char *buf,size_t bytesof_buf){
    struct tiff_reader *self=containerof(r,struct tiff_reader,R);
    if(ScanImageTiffReader_GetData(&self->T,buf,bytesof_buf))
        return buf;
    return 0;
}

static const char* GetLastError(struct Reader *r){
    struct tiff_reader *self=containerof(r,struct tiff_reader,R);
    return self->T.log;
}

struct Reader *Reader_Open_Tiff(const char* filename) {
    struct tiff_reader *self=(struct tiff_reader*)malloc(sizeof(*self));
    if(self) {
        self->R=(struct Reader){
            .Close=Close,
            .GetShape=GetShape,
            .GetChunkShape=GetChunkShape,
            .GetSubvolumeSizeBytes=GetSubvolumeSizeBytes,
            .GetSubvolume=GetSubvolume,
            .GetLastError=GetLastError
        };
        self->T=ScanImageTiffReader_Open(filename);
    }
    return &self->R;
}