#ifndef H_FILESERIES_API
#define H_FILESERIES_API

#include <stddef.h>
#include "nd.h"
#include "reader.h"

#ifdef __cplusplus
extern "C" {
#endif

    struct FileSeriesReader {
        void *handle;        /**< A pointer to the (abstract) file context */
        const char *log;     /**< Points to a buffer with error information if something goes wrong. */
    };
    typedef struct FileSeriesReader FileSeriesReader;

    /// \param filename The path to a lua script that returns a table mapping
    ///                 indexes to filenames.
    /// \param nargs    The number of string arguments in args
    /// \param args     An array of nargs strings.
    ///                 These string arguments are passed to the lua script.
    FileSeriesReader FileSeriesReader_Open    (const char* filename,int nargs,const char** args);
    void             FileSeriesReader_Close   (FileSeriesReader *r);
    struct nd        FileSeriesReader_GetShape(FileSeriesReader *r);

    // The requested offset,shape might not be what is returned.
    // The shape parameter is modified to reflect the output shape.
    // the offset parameter is modified to reflect the origin of the output volume
    size_t           FileSeriesReader_GetSubvolumeSizeBytes(FileSeriesReader *r,struct nd *shape);
    int              FileSeriesReader_GetSubvolume(FileSeriesReader *r,int64_t *offset,struct nd *shape,unsigned char *buf,size_t bytes_of_buf);

    //
    // READER API
    //

    struct Reader *Reader_Open_FileSeries(const char* filename);
    struct Reader *Reader_Open_FileSeries_Args(const char* filename,int nargs,const char **args);


#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* header guard */
