#ifndef H_NGC_GLASS
#define H_NGC_GLASS

#ifdef  __cplusplus
extern "C" {
#endif

/// The glass api functions are used by the host (main.c) which is responsible
/// for implementing the main loop and making sure the right inputs/events get
/// mapped to the right behaviors.

const char* glass_version();

void glass_init(
    void(*log)(int is_error,const char *file,int line,const char* function,const char *fmt,...));
void glass_update();
void glass_draw();
void glass_resize(unsigned w,unsigned h);

/// Draggable behavior

void glass_drag_start(int x,int y);
int  glass_is_dragging();
void glass_drag_update(int x,int y);
void glass_drag_stop();

#ifdef  __cplusplus
}
#endif
#endif /* ifndef H_NGC_GLASS */

