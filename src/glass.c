#include <stdlib.h> // malloc
#include <string.h> // memset
#include "glass.h"
#include <pixelfont/src/mingl.h>
#include "show/imshow.h"
#include "show/subvolshow.h"
#include "pixelfont.h"
#include "tiff.reader.api.h"
#include "controls/slider.h"
#include "controls/rotator.h"
#include "controls/behavior/observable.h"
#include "io/fileseries.h"
#include "io/cache.h"
#include "io/reader.h"

#include <math.h>

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))
#define countof(e) (sizeof(e)/sizeof(*(e)))

#define LOG(...) app.logger(0,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define ERR(...) app.logger(1,__FILE__,__LINE__,__FUNCTION__,__VA_ARGS__)
#define CHECK(e) do {if(!((e))) {ERR("Expression was false.\n%s\n",#e); goto Error;}} while(0)

typedef void(*logger_t)(int is_error,const char *file,int line,const char* function,const char *fmt,...);

struct App {
    int nframes;
    logger_t logger;

    LARGE_INTEGER frame_clock;
    float dt_us;
    float time_us;

    struct nd shape;
    CachedReader reader;
    enum CachedReaderStatus last_cache_status;

    // drag and drop (internal)
    struct dlist draggables;
    struct dlist dragging;

    // viewport resize
    struct dlist viewport_listeners;

    struct sliders {
        struct slider
            a,b,c,
            contrast_min,
            contrast_max,
            z,
            t,
            dof;
    } slider;


    struct {
        struct listener 
            ab,bc,ca,
            rotator_to_subvshow,
            subvshow_to_rotator;
    } listeners;

    struct rotator rotator;
    struct subvshow subvshow;
    int last_tm,cache_dt;

    const char *err_msg;
} app;

const char* glass_version() {
    return "Glass version " GIT_TAG GIT_HASH;
}

static void load_stack(){
    struct Reader *source_reader=Reader_Open_FileSeries("resource/klb_test.lua");
    // struct Reader *source_reader=Reader_Open_FileSeries("resource/timeseries.lua");
    //struct Reader *source_reader=Reader_Open_FileSeries("resource/MB_104y_247_ext_w_s5_20170214_1dFB_00001.lua");
    app.shape=Reader_GetShape(source_reader);
    struct nd shape=app.shape;
    shape.dims[2]=1;
    shape.dims[3]=1;

    app.reader=CachedReader_Open(source_reader,
                                 malloc(1ULL<<33),
                                 1ULL<<33);
}

// slider -> slider
static void notify_slider_to_slider(struct listener *listener) {
    const struct slider *source_slider=containerof(listener->source,struct slider,observable);
    struct slider *self=containerof(listener->sink,struct slider,observable);
    slider_set(self,source_slider->v*0.75f);
}

// rotator -> rotatable
static void notify_rotator_to_rotatable(struct listener *listener) {
    const struct rotator *source=containerof(listener->source,struct rotator,observable);
    struct rotatable *sink=containerof(listener->sink,struct rotatable,observable);
    memcpy(sink->mat,source->mat,9*sizeof(float));
    observable_mark(&sink->observable);
    OutputDebugStringA("rotator -> rotatable");
}

// rotatable -> rotator
static void notify_rotatable_to_rotator(struct listener *listener) {
    const struct rotatable *source=containerof(listener->source,struct rotatable,observable);
    struct rotator *sink=containerof(listener->sink,struct rotator,observable);
    memcpy(sink->mat,source->mat,9*sizeof(float));
    rotator_notify(sink);
    OutputDebugStringA("rotatable -> rotator");
}

void glass_init(logger_t logger) {
    memset(&app,0,sizeof(app));
    app.logger=logger;
    app.last_tm=-1;
    mingl_init(logger);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

    QueryPerformanceCounter(&app.frame_clock);
    
    dlist_init(&app.draggables);
    dlist_init(&app.dragging);
    dlist_init(&app.viewport_listeners);

    load_stack();
#if 0
    imshow_rect(50,50,(float)app.shape.dims[0],(float)app.shape.dims[1]);
    imshow_contrast(nd_i16,0,1000);
#endif

    rotator_init(&app.rotator,40,10);
    rotator_rect(&app.rotator,10,100,100,100);
    dlist_insert(&app.draggables,&app.rotator.draggable.item);
    dlist_insert(&app.viewport_listeners,&app.rotator.view.viewport_listener.item);

    subvshow_init(&app.subvshow);
    subvshow_rect(&app.subvshow,120,100,512,512);
    subvshow_contrast(&app.subvshow,nd_i16,0,1000);
    dlist_insert(&app.draggables,&app.subvshow.rotatable_cube.rotatable.draggable.item);
    dlist_insert(&app.viewport_listeners,&app.subvshow.view.viewport_listener.item);

    {
        struct slider *s=&app.slider.contrast_min;
        slider_init(s);
        s->v=0;
        s->v_min=-1;
        s->v_max=400;
        slider_rect(s,10,30,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }

    {
        struct slider *s=&app.slider.contrast_max;
        slider_init(s);
        s->v=1000;
        s->v_min=-1;
        s->v_max=1000;
        slider_rect(s,10,40,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }

    {
        struct slider *s=&app.slider.z;
        slider_init(s);
        s->v_min=0;
        s->v_max=max(s->v_min,(float)app.shape.dims[2]-1); // be wary of coercion
        s->v=0;
        slider_rect(s,10,50,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }

    {
        struct slider *s=&app.slider.t;
        slider_init(s);
        s->v_min=0;
        s->v_max=max(s->v_min,(float)app.shape.dims[3]-1); // be wary of coercion
        s->v=0.0f; //0.5f*(s->v_max+s->v_min);
        slider_rect(s,10,60,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }
    
    {
        struct slider *s=&app.slider.dof;
        slider_init(s);
        s->v_min=0;
        s->v_max=1;
        s->v=1;
        slider_rect(s,10,70,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }
//a,b,c
    {
        struct slider *s=&app.slider.a;
        slider_init(s);
        s->v=0;
        s->v_min=0;
        s->v_max=1;
        slider_rect(s,10,200,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }
    {
        struct slider *s=&app.slider.b;
        slider_init(s);
        s->v=0;
        s->v_min=0;
        s->v_max=1;
        slider_rect(s,10,210,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }
    {
        struct slider *s=&app.slider.c;
        slider_init(s);
        s->v=0;
        s->v_min=0;
        s->v_max=1;
        slider_rect(s,10,220,100,8);
        dlist_insert(&app.draggables,&s->draggable.item);
        dlist_insert(&app.viewport_listeners,&s->viewport_listener.item);
    }
    {
        listener_connect(
            &app.listeners.ab,
            &app.slider.a.observable,
            &app.slider.b.observable,
            notify_slider_to_slider);
        listener_connect(
            &app.listeners.bc,
            &app.slider.b.observable,
            &app.slider.c.observable,
            notify_slider_to_slider);
        listener_connect(
            &app.listeners.ca,
            &app.slider.c.observable,
            &app.slider.a.observable,
            notify_slider_to_slider);

        listener_connect(
            &app.listeners.rotator_to_subvshow,
            &app.rotator.observable,
            &app.subvshow.rotatable_cube.rotatable.observable,
            notify_rotator_to_rotatable);
        listener_connect(
            &app.listeners.subvshow_to_rotator,
            &app.subvshow.rotatable_cube.rotatable.observable,
            &app.rotator.observable,
            notify_rotatable_to_rotator);

    }
}

static int mat33_is_equal(const float *a, const float *b) {
    for(int i=0;i<9;++i)
        if(a[i]!=b[i])
            return 0;
    return 1;
}

static size_t nbytes(const struct nd * shape) {
    const size_t bpp[]={1,2,4,8,1,2,4,8,4,8};
    return shape->strides[shape->ndim]*bpp[shape->type];
}

static void restride(struct nd *s) {
    s->strides[0]=1;
    for(unsigned i=0; i < s->ndim; ++i)
        s->strides[i+1]=s->dims[i]*s->strides[i];
}

static void draw_timepoint(int64_t t) {
    
    unsigned char *buf;
    enum CachedReaderStatus status;
    if(!app.reader.handle)
        return;
    struct nd fullshape=CachedReader_GetShape(&app.reader);    
    struct nd unit=CachedReader_GetUnitShape(&app.reader);

    subvshow_beg(&app.subvshow,&fullshape);

    app.cache_dt=-1; // -1 indicates loading
    // for(uint64_t z=0;z<fullshape.dims[2];z+=unit.dims[2]) {
    { uint64_t z=32;
    for(uint64_t y=0;y<fullshape.dims[1];y+=unit.dims[1]) {
    for(uint64_t x=0;x<fullshape.dims[0];x+=unit.dims[0]) {
    // {{{ uint64_t x=288,y=960,z=56;
        int64_t offset[countof(app.shape.dims)]={[0]=x,[1]=y,[2]=z,[3]=t};
        struct nd shape=CachedReader_GetSubvolume(&app.reader,offset,&buf,&status);

        if(!buf) {
            app.err_msg=app.reader.log;
        } else if(status==CachedReaderStatus_Done) {
            if( t!=app.last_tm )
                subvshow_clear(&app.subvshow);
            app.last_tm=t;
            app.cache_dt=0; // 0 indicates can start precaching
            subvshow_add(&app.subvshow,
                (float[3]){x,y,z},
                &shape,buf);
        } 

    }}}

    subvshow_end(&app.subvshow);
    app.last_cache_status=status;
}

static void precache_timepoint(int64_t t) {
    // TODO: ideally would stop this once the cache was full.
    int64_t offset[countof(app.shape.dims)]={[3]=t};
    CachedReader_PreCache(&app.reader,offset);
}

#include <math.h>
void glass_update() {
    static float timer_last_tic_us=0;
    app.nframes++;

    LARGE_INTEGER clock,freq;
    QueryPerformanceCounter(&clock);
    QueryPerformanceFrequency(&freq);
    app.dt_us=(1e6f*(clock.QuadPart-app.frame_clock.QuadPart))/freq.QuadPart;
    app.frame_clock=clock;

    app.time_us+=app.dt_us;

    observables_update();

#if 0
    imshow_contrast(nd_i16,app.slider.contrast_min.v,app.slider.contrast_max.v);
#endif

    subvshow_contrast(&app.subvshow,nd_i16,app.slider.contrast_min.v,app.slider.contrast_max.v);
    // if( ((int)app.slider.t.v)!=app.last_tm || app.subvshow.view.is_dirty) {
    if(1) {
        draw_timepoint(app.slider.t.v);
    } else {
        // selected time point didn't change...maybe pre-cache
        if(app.cache_dt>=0 && app.slider.t.v_max>=1 ) {
            app.cache_dt++;
            precache_timepoint( ((int)app.slider.t.v+app.cache_dt)%(int)app.slider.t.v_max );
        }
    }
}

void glass_draw() {
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT);
    float fps=1e6f/app.dt_us;
    text(10,10,"FPS: %f",fps);

    text(120,30,"Min: %f",app.slider.contrast_min.v);
    text(120,40,"Max: %f",app.slider.contrast_max.v);
    text(120,50,"Z: %f",app.slider.z.v);
    text(120,60,"T: %f",app.slider.t.v);
    text(120,70,"Field depth: %f",app.slider.dof.v);

    if(app.last_cache_status==CachedReaderStatus_Loading)
        text(256,10,"Loading");

    subvshow_set_view_plane(&app.subvshow,
        app.slider.z.v/app.slider.z.v_max,
        app.slider.dof.v);
    subvshow_draw(&app.subvshow);

    slider_draw(&app.slider.contrast_min);
    slider_draw(&app.slider.contrast_max);
    slider_draw(&app.slider.z);
    slider_draw(&app.slider.t);
    slider_draw(&app.slider.dof);
        
    slider_draw(&app.slider.a);
    slider_draw(&app.slider.b);
    slider_draw(&app.slider.c);

    rotator_draw(&app.rotator);

    if(app.err_msg)
        text(10,256,app.err_msg);
}

void glass_resize(unsigned w,unsigned h) {
    glViewport(0,0,w,h);

    {
        struct dlist_iterable it;
        for(dlist_beg(&it,&app.viewport_listeners);!dlist_is_done(&it);dlist_next(&it)) {
            struct viewport_listener *self=containerof(it.cur,struct viewport_listener,item);
            self->set(self,(float)w,(float)h);
        }
    }

    text_viewport(w,h);
    imshow_viewport(w,h);
}

//
// DRAG AND DROP (Internal)
//

/// Iterates through a registered list of draggable items.
/// Any items that are hit are activated for dragging.
void glass_drag_start(int x,int y) {
    struct dlist_iterable it;
    for(dlist_beg(&it,&app.draggables);!dlist_is_done(&it);) {
        struct draggable *self=containerof(it.cur,struct draggable,item);
        dlist_next(&it); // do this here because we fiddle with the item's list membership when hit
        if(self->hit(self,x,y)) {
            dlist_remove(&app.draggables,&self->item);
            dlist_insert(&app.dragging,&self->item);
            self->x0=(float)x;
            self->y0=(float)y;
            // return; // uncomment this if we only want to allow dragging the first hit item, otherwise all items hit get dragged
        }
    }
}

/// returns true if any draggable is currently
/// involved in a drag.
int glass_is_dragging() {
    return !dlist_is_empty(&app.dragging);
}

void glass_drag_update(int x,int y) {
    struct dlist_iterable it;
    for(dlist_beg(&it,&app.dragging);!dlist_is_done(&it);dlist_next(&it)){
        struct draggable *self=containerof(it.cur,struct draggable,item);
        self->set(self,(float)self->x0,(float)self->y0,(float)x,(float)y);
    }
}

void glass_drag_stop() {
    struct dlist_item
        *a=app.draggables.cur,
        *b=app.draggables.cur->next,
        *c=app.dragging.sentinal.next,
        *d=app.dragging.sentinal.prev;

    // splice out
    c->prev->next=d->next;
    d->next->prev=c->prev;
    app.dragging.cur=app.dragging.sentinal.next;

    // splice in
    a->next=c;
    c->prev=a;
    d->next=b;
    b->prev=d;
    app.draggables.cur=app.draggables.sentinal.next;
}

/* NOTES

Draggable
---------

One anoying thing is that the anchor point is stored inside
struct draggable.  It would be more natural for that to be
part of the owning object.

One way to change things would be to just point to the (x,y)
anchor.  Another way would be to use set() and get() methods.

One consideration is the incorporation of constraints later.
This makes me want to lean toward set/get.

Mixin verses Entity table
-------------------------

Right now I'm using a "mixin" strategy (really, just inheritance)
to manage behaviors.  Frequeuntly this means iterating over
lists of items with a behavior in order to implement a system.

Alternatively, I could have a table in each object that has
entries for behaviors.  A system might iterate over all objects
filtering for the right behavior (or a combination).

It would ease up on the need for all the lists (maybe).
Actually, I'd probably want an inverse mapping that would end
up being the same thing.  Maybe I'd just have a more uniform
interface over things.  Not sure.

Again, maybe this is an interesting thing to think about in
terms of how a draggable's set() would interact with some
set of constraints.  I guess this is just a question of
composition.


*/
