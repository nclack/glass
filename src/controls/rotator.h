#pragma once
#ifndef H_NGC_3DROTATION_CONTROL
#define H_NGC_3DROTATION_CONTROL

#include "behavior/draggable.h"
#include "behavior/viewport.h"
#include "behavior/observable.h"

#ifdef __cplusplus
#extern "C" {
#endif

    struct rotator {
        struct draggable draggable;
        struct observable observable;
        float drag_start_orientation[9];   // Used to save the rotator orientation when drag starts. (a copy of mat)
        int   drag_hit_rotor;              // Index of the last rotor to get hit by the drag-and-drop hit test
        struct rotor {
            float *x,*y,*z; // z is the axis of rotation.  x and y are orthogonal in the plane of rotation.
            float cx,cy;    // the center about which the rotor rotates
            float r,dr;     // hist zone is a distance (r-dr,r+dr) away from (cx,cy)
            float theta;    // the rotation set point
            int idx;
        } rotors[3];
        float mat[9]; // operating in row space (column major). mat[0..2] is x axis. mat[3..5] is y axis. mat[6..8] is z axis 

        struct rotator_view {
            struct display { //displays accumulator as a textured quad
                unsigned program;
                unsigned vbo,vao;
                struct display_uniforms {
                    unsigned size;
                } id;
            } display;
            struct accumulator { // Render to texture
                int program;
                unsigned tex[2];
                unsigned fbo,vbo,vao;
                unsigned counts;
                unsigned w,h; // size of accumulator texture
                struct accumulator_uniforms {
                    unsigned counts;
                    unsigned vx,vy,vz;
                    unsigned size;
                    unsigned minr,maxr;
                } id;
            } accumulator;
            struct viewport_listener viewport_listener;
        } view;

        //
        float last_dth;
        float last_dth_ortho;
    };

    void rotator_init(struct rotator *rotator,float r, float dr);
    void rotator_draw(struct rotator *rotator);
    void rotator_rect(struct rotator *rotator, float x,float y,float w,float h);


    void rotator_notify(struct rotator *rotator);
#ifdef __cplusplus
}
#endif

#endif // H_NGC_3DROTATION_CONTROL
