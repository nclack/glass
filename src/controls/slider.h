#pragma once
#ifndef H_NGC_SLIDER
#define H_NGC_SLIDER

#include "behavior/draggable.h"
#include "behavior/viewport.h"
#include "behavior/observable.h"

#ifdef __cplusplus
#extern "C" {
#endif

    struct slider {
        float x,y,w,h;
        float v,v_min,v_max;
        struct draggable draggable;
        struct observable observable;
        struct viewport_listener viewport_listener;
        float ax,ay; //anchor for dragging

        unsigned vbo,vao;
    };

    void slider_init(struct slider *slider);
    void slider_draw(struct slider *slider);
    void slider_rect(struct slider *slider, float x,float y,float w,float h);
    void slider_set(struct slider *slider,float v);
    void slider_set_range(struct slider *slider,float v_min,float v,float v_max);
    //void slider_viewport(struct slider *slider, int w,int h);


#ifdef __cplusplus
}
#endif

#endif

/* NOTES
 * 
 * First thing is to just draw a rect on a line.
 * 
 * Second thing will be to add some layout and a text display.
 * 
 * TODO nudge mousewheel
 * TODO nudge arrow keys
 * TODO nudge action (eg by a button)
 * TODO click to set
 *      
 * TODO ? how to get precision over large range (eg for contrast)
 *        Maybe a delayed hover zooms in?
 */