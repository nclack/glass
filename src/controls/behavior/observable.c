#include "observable.h"
#include <stddef.h>

//#define INSTRUMENT
#ifdef INSTRUMENT
#define INST_EXPR(...) __VA_ARGS__
#else
#define INST_EXPR(...)
#endif

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

static struct globals {
    unsigned is_inited;
    uint64_t clock;
    struct dlist observables;
} global;

static void maybe_init_globals() {
    if(global.is_inited)
        return;
    dlist_init(&global.observables);
    global.is_inited=1;
}

//

void observable_init(struct observable *self) {
    maybe_init_globals();
    dlist_init(&self->observers);
    dlist_insert(&global.observables,&self->item);
}

void observable_mark(struct observable *self) {
    maybe_init_globals();
    self->timestamp=global.clock+1;
}

void listener_connect(struct listener *self,
                      struct observable *source, 
                      struct observable *sink,
                      void (*notify)(struct listener*)) 
{
    maybe_init_globals();
    self->timestamp=source->timestamp;
    self->source=source;
    self->sink=sink;
    self->notify=notify;
    dlist_insert(&source->observers,&self->item);
}




#include <stdio.h>

// updates the network of listeners for all observables
void observables_update() {
    maybe_init_globals();
    INST_EXPR(int nloops=0,nobs=0,nlist=0,nnotifies=0);
    struct dlist_iterable it0;
    int any;
    do {
        INST_EXPR(nloops++);
        any=0;
        for(dlist_beg(&it0,&global.observables);!dlist_is_done(&it0);dlist_next(&it0)) {
            struct observable *o=containerof(it0.cur,struct observable,item);
            INST_EXPR(nobs++);
            // start w observers that have timestamps ahead of clock
            if(o->timestamp>global.clock) {
                struct dlist_iterable it1;
                for(dlist_beg(&it1,&o->observers);!dlist_is_done(&it1);dlist_next(&it1)) {
                    struct listener *l=containerof(it1.cur,struct listener,item);
                    INST_EXPR(nlist++);
                    // start w observers that have timestamps ahead of clock
                    if(l->timestamp<o->timestamp) {
                        if(l->sink==NULL || l->sink->timestamp<o->timestamp) {
                            INST_EXPR(nnotifies++);
                            any=1;
                            l->notify(l);
                            l->timestamp=o->timestamp;
                        }
                    }
                }
            }
        }
    } while(any);
    // update global.clock to most recent timestamp
    global.clock++;
#ifdef INSTRUMENT
    if(nlist>0){
        char buf[1024]={0};
        snprintf(buf,sizeof(buf),"nloops=%5d,nobs=%5d,nlist=%5d,nnotifies=%5d\n",nloops,nobs,nlist,nnotifies);
        OutputDebugStringA(buf);
    }
#endif
} 