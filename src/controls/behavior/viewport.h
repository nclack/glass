#ifndef H_NGC_VIEWPORT
#define H_NGC_VIEWPORT

#include "dlist.h"

#ifdef  __cplusplus
extern "C" {
#endif

/// Draggable behavior
///
/// Controls implement struct draggable and register those with the glass
/// controller.
///
/// The glass_drag api functions are used by the host which is responsible
/// for making sure the right inputs/events get mapped to the right 
/// behaviors.

struct viewport_listener {
    struct dlist_item item;                                      // (internal) Handle used for list manipulation
    float w,h;                                                   // (internal) last viewport size
    void(*set)(struct viewport_listener *self,float w, float h); // Updates the viewport size
};

#ifdef  __cplusplus
}
#endif
#endif /* ifndef H_NGC_VIEWPORT */

