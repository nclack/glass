#ifndef H_NGC_DRAGGABLE
#define H_NGC_DRAGGABLE

#include "dlist.h"

#ifdef  __cplusplus
extern "C" {
#endif

/// Draggable behavior
///
/// Controls implement struct draggable and register those with the glass
/// controller.
///
/// The glass_drag api functions are used by the host which is responsible
/// for making sure the right inputs/events get mapped to the right 
/// behaviors.

struct draggable {
    struct dlist_item item;                                     // (internal) Handle used for list manipulation
    float x0,y0;                                                // (internal) Where the drag was started
    int (*hit)(const struct draggable* self,int x,int y);       // Hit-test function. Returns 1 if (x,y) hits the object, and 0 otherwise.
    void(*set)(const struct draggable *self,float x0, float y0, float x1,float y1);   // Updates the draggable item with a drag from (x0,y0) to (x1,y1)    
};

#ifdef  __cplusplus
}
#endif
#endif /* ifndef H_NGC_DRAGGABLE */

