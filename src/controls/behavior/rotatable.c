#include "rotatable.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h> // memcpy
#include <stddef.h>

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

static float dot3(const float *a, const float *b) {
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

static void lift(const struct plane* plane,float dx,float dy,float *out,float *outz) {
    // lift to plane orthogonal to rotation axis
    float r[3]={dx,dy,0};
    const float *x=plane->T,
                *y=plane->T+3,
                *z=plane->T+6; // plane normal
    r[2]=plane->offset-(z[0]*r[0]+z[1]*r[1])/z[2];
    if(out) {
        out[0]=dot3(r,x);
        out[1]=dot3(r,y);
    }
    if(outz) 
        *outz=r[2];
}

static void mul33(float *out, const float *a, const float *b) {
    for(int c=0;c<3;++c) {
        for(int r=0;r<3;++r) {
            float acc=0.0f;
            for(int i=0;i<3;++i)
                acc+=a[i+3*r]*b[c+3*i];
            out[c+3*r]=acc;
        }
    }
}

static void mul33_cpy(float* out,const float *a,const float *b) {
    float t[9];
    mul33(t,a,b);
    memcpy(out,t,sizeof(t));
}

static void fmad33_ip(float *out, const float a, const float *b) {
    for(int c=0;c<3;++c)
        for(int r=0;r<3;++r)
            out[c+3*r]+=a*b[c+3*r];
}

// Generates a rotation matrix, out, that rotates by theta about
// the vector k. Assumes k is normalized.
//
// out is a 3x3 matrix allocated/owned by the caller.
static void rotmat(float* out,const float k[3],const float theta) {
    // https://en.wikipedia.org/wiki/Rodrigues_rotation_formula
    const float K[9]={
                0, -k[2], k[1],
             k[2],     0,-k[0],
            -k[1],  k[0],    0};
    float K2[9];
    mul33(K2,K,K);
    const float e[]={1,0,0,0,1,0,0,0,1};
    memcpy(out,e,sizeof(e));
    fmad33_ip(out,sinf(theta),K);
    fmad33_ip(out,1.0f-cosf(theta),K2);
}

// exchanges rows a and b in a 3x3 matrix
static void perm33(float *T,int a, int b) {
    float *ra=T+3*a,
          *rb=T+3*b;
    for(int i=0;i<3;++i) {
        float t=ra[i];
        ra[i]=rb[i];
        rb[i]=t;
    }
}

static void normalize3(float *a) {
    const float n=sqrtf(a[0]*a[0]+a[1]*a[1]+a[2]*a[2])+1e-6f;
    a[0]/=n;
    a[1]/=n;
    a[2]/=n;
}

static float norm3(float *a) {
    return sqrtf(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]);
}

static void cross(float out[3],const float a[3],const float b[3]) {
    out[0]=a[1]*b[2]-a[2]*b[1];
    out[1]=a[2]*b[0]-a[0]*b[2];
    out[2]=a[0]*b[1]-a[1]*b[0];
}

static float clamp(const float v, const float min, const float max) {
    return (v<min)?min:((v>max)?max:v);
}



static int cube_hit(const struct draggable *self, int x, int y) {
    struct rotatable *rotatable=containerof(self,struct rotatable,draggable);
    struct rotatable_cube *cube=containerof(rotatable,struct rotatable_cube,rotatable);
    // the cube has 6 planes.  The center of the cube is the origin.
    // lift the point to each plane and test z.
    // The plane with the largest z with in-bounds (x,y) is hit.
    struct plane planes[6] ={
        {.T={1,0,0,0,1,0,0,0,1}, .offset=cube->size[0]/2.0f},
        {.T={1,0,0,0,1,0,0,0,1}, .offset=cube->size[0]/2.0f},
        {.T={1,0,0,0,1,0,0,0,1}, .offset=cube->size[1]/2.0f},
        {.T={1,0,0,0,1,0,0,0,1}, .offset=cube->size[1]/2.0f},
        {.T={1,0,0,0,1,0,0,0,1}, .offset=cube->size[2]/2.0f},
        {.T={1,0,0,0,1,0,0,0,1}, .offset=cube->size[2]/2.0f},
    };
    // Initially rotate planes to form sides of cube
    {
        perm33(planes[2].T,0,2);
        perm33(planes[3].T,0,2);
        for(int i=0;i<3;++i)
            planes[3].T[6+i]*=-1; // flip normal
        perm33(planes[4].T,1,2);
        perm33(planes[5].T,1,2);
        for(int i=0;i<3;++i)
            planes[5].T[6+i]*=-1; // flip normal
    }
    // find hit plane
    int argmax=-1;
    {
        float maxz=0;
        for(int i=0;i<6;++i) {
            float r[2],z;
            mul33_cpy(planes[i].T,rotatable->mat,planes[i].T);
            lift(planes+i,x-cube->center[0],y-cube->center[1],r,&z);
            if(   (fabs(r[0])<=(cube->size[0]/2.0f))
                &&(fabs(r[1])<=(cube->size[1]/2.0f))
                &&(argmax<0 || z>maxz)) 
            {
                maxz=z;
                argmax=i;
            }  
        }
    }
    if(argmax>=0) {
        cube->hit_plane=planes[argmax];
        memcpy(rotatable->start,rotatable->mat,9*sizeof(float));
        return 1;
    }
    return 0;
}

static void cube_set(const struct draggable *self,float x0,float y0,float x1,float y1) {
    struct rotatable *rotatable=containerof(self,struct rotatable,draggable);
    struct rotatable_cube *cube=containerof(rotatable,struct rotatable_cube,rotatable);
    if(x0==x1 && y0==y1)
        return;
    // lift (x0,y0) to hit plane to generate a 3d point.  This determines the radius of the 
    // sphere of rotation.  Lift (x1,y1) to that sphere and use cross product to compute 
    // rotation matrix.
    float r0[3]={x0-cube->center[0],y0-cube->center[1],0},
          radius2,
          r1[3]={x1-cube->center[0],y1-cube->center[1],0},
          k[3];
    lift(&cube->hit_plane,r0[0],r0[1],0,r0+2);
    radius2=r0[0]*r0[0]+r0[1]*r0[1]+r0[2]*r0[2];
    r1[2]=sqrtf(clamp(radius2-r1[0]*r1[0]-r1[1]*r1[1],0,radius2));
    normalize3(r0);
    normalize3(r1);
    cross(k,r0,r1);
    const float
        n  =sqrtf(k[0]*k[0]+k[1]*k[1]+k[2]*k[2]),
        dth=asinf(clamp(n,-1,1));
    normalize3(k);
    float T[9];
    rotmat(T,k,dth);
    mul33(rotatable->mat,rotatable->start,T);

    observable_mark(&cube->rotatable.observable);
}

// updates the cube geometry
void rotatable_cube_set(struct rotatable_cube *self,int *origin,int *dims) {
    for(int i=0;i<3;++i) {
        self->size[i]=(float)dims[i];
        self->center[i]=origin[i]+dims[i]*0.5f;
    }
}

void rotatable_cube_init(struct rotatable_cube *self,int *origin,int *dims) {
    observable_init(&self->rotatable.observable);    
    rotatable_cube_set(self,origin,dims);
    float I[9]={1,0,0,0,1,0,0,0,1};
    memcpy(self->rotatable.mat,I,sizeof(I));
    self->rotatable.draggable.hit=cube_hit;
    self->rotatable.draggable.set=cube_set;
}

