#ifndef H_NGC_ROTATABLE
#define H_NGC_ROTATABLE

#include "draggable.h"
#include "observable.h"

#ifdef  __cplusplus
extern "C" {
#endif

struct rotatable {
    struct draggable draggable;
    float start[9], // stores the orientation when the drag is started
          mat[9];   // current orientation: operating in row space (column major). mat[0..2] is x axis. mat[3..5] is y axis. mat[6..8] is z axis
    struct observable observable;
};

struct plane { 
    float T[9],   // T is a non-scaling transform of the plane normal
          offset; // offset of the plane from the origin along the normal
};

struct rotatable_cube {
    struct rotatable rotatable;
    float center[3], size[3];
    struct plane hit_plane;
};

void rotatable_cube_init(struct rotatable_cube *self,int *origin,int *dims);
void rotatable_cube_set(struct rotatable_cube *self,int *origin,int *dims);

#ifdef  __cplusplus
}
#endif
#endif /* ifndef H_NGC_ROTATABLE */