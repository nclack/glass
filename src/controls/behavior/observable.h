#pragma once
#ifndef H_NGC_OBSERVABLE
#define H_NGC_OBSERVABLE

#include "../../dlist.h"
#include <stdint.h>

#ifdef  __cplusplus
extern "C" {
#endif

struct observable {
    struct dlist_item item;
    struct dlist observers;
    uint64_t timestamp;
};

struct listener {
    struct dlist_item item;
    uint64_t timestamp;
    const struct observable *source;
    struct observable *sink;
    void (*notify)(struct listener* listener);
};

void observable_init  (struct observable *self);
void observable_mark  (struct observable *self);

void listener_connect(struct listener *self,
                      struct observable *source, 
                      struct observable *sink, ///< can be null
                      void (*notify)(struct listener*));

void observables_update(); // updates the network of listeners for all observables

#ifdef  __cplusplus
}
#endif
#endif /* ifndef H_NGC_OBSERVABLE */

/*

NOTES

Random

    The "sink" only needs to be observable because of the timestamp.  Otherwise it
    could be null*.  I feel like we're usually connecting observables though.

    Probably don't need listener timestamp? ... Ok, maybe we do

Property graph

    Encoding dependencies between properties sets up a directed graph.

    Listeners represent the directed edges.

    Here, the nodes in the graph are defined by an observable and one or
    more listeners.  The notify() callback ties them together in the sense
    that when it gets called it may update some observables.
    

Example use

    struct slider {
        ...
        struct observable observable;
        ...
    };

    struct slider_listener {
        struct slider *owner;
        struct listener listener;
    };

    static void notify_slider_slider(struct listener *listener,const struct observable *source) {
        const struct slider *source_slider=containerof(source,struct slider,observable);
        struct slider_listener *self=containerof(listener,struct slider_listener,listener);
        if(self->owner->observable.timestamp<listener->timestamp)
            slider_set(self->owner,source_slider->v*0.75);    
    }

*/