#include "slider.h"
#include "../pixelfont/src/mingl.h"

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct verts{ float x,y,u,v; };

static const char FRAG[]=GLSL(
    in  vec2 tex_;
    out vec4 color;
    uniform vec2 wh; //width and height in pixels

    void main(){
        vec2 dr=vec2(1,1)/wh;
        bool tx=(dr.x>=tex_.s)||(tex_.s>=(1-dr.x));
        bool ty=(dr.y>=tex_.t)||(tex_.t>=(1-dr.y));
        color=vec4(tx||ty,tx||ty,tx||ty,tx||ty);
    }
);

static const char VERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    uniform vec2 size;
    out vec2 tex_;

    void main(){
        gl_Position=vec4(2*(vert/size)-1,0,1.0f);
        gl_Position.y=-gl_Position.y;
        tex_=tex;
    }
);

// all sliders use the same program/viewport size
// so we just update a global state
static struct ctx {
    unsigned program;
    struct id {
        unsigned size,wh;
    } id;
} CTX, *ctx_;

static void update_geometry(const struct slider *slider);

static void viewport_resize(struct viewport_listener *self,float w, float h) {
    // all sliders use the same program/viewport size
    // so we just update a global state
    glUseProgram(ctx_->program);
    glUniform2f(ctx_->id.size,w,h);
    glUseProgram(0);
}

static void ctx_init() {
    struct ctx *ctx=&CTX;

    // Shader 
    ctx->program = mingl_new_program(FRAG,sizeof(FRAG),VERT,sizeof(VERT),0,0);
    ctx->id.size = glGetUniformLocation(ctx->program,"size");
    ctx->id.wh   = glGetUniformLocation(ctx->program,"wh");
    glUseProgram(ctx->program);
    {
        GLint sz[4]={0};
        glGetIntegerv(GL_VIEWPORT,sz);
        glUniform2f(ctx->id.size,(float)sz[2],(float)sz[3]);
    }
    glUseProgram(0);

    if(!mingl_check())
        goto OpenGLError;
    ctx_=ctx;
    return;
OpenGLError:
    ctx_=0;
}

static void get_handle_rect(const struct slider *slider,float *x0,float *y0,float *x1,float *y1) {
    const int is_tall=slider->w<slider->h;
    const float
        sz=0.5f*(is_tall?slider->w:slider->h),
        normv=(slider->v-slider->v_min)/(slider->v_max-slider->v_min),
        effw=slider->w-2*sz,
        effh=slider->h-2*sz;
    *x0=slider->x+(!is_tall?(normv*effw):0.0f);
    *y0=slider->y+( is_tall?(normv*effh):0.0f);
    *x1=*x0+2*sz;
    *y1=*y0+2*sz;
}

static int slider_drag_hit(const struct draggable* self_, int x, int y) {
    struct slider *self=containerof(self_,struct slider,draggable);
    float x0,y0,x1,y1;
    get_handle_rect(self,&x0,&y0,&x1,&y1);

    // set anchor
    {
        const int is_tall=self->w<self->h;
        const float
            sz=0.5f*(is_tall?self->w:self->h),
            normv=(self->v-self->v_min)/(self->v_max-self->v_min);
        self->ax=self->x+(!is_tall?(normv*self->w):0.0f);
        self->ay=self->y+( is_tall?(normv*self->h):0.0f);
    }

    return x>=x0 && x<x1 && y>=y0 && y<y1;
}

static void clamp(float* v,float a,float b) {
    *v=*v<b?*v:b;
    *v=*v>a?*v:a;
}

static void slider_drag_set(const struct draggable *self_,float x0,float y0,float x1,float y1) {
    struct slider *slider=containerof(self_,struct slider,draggable);
    const float 
        x=slider->ax+x1-x0,
        y=slider->ay+y1-y0;

    const int is_tall=slider->w<slider->h;
    const float 
        sz=0.5f*(is_tall?slider->w:slider->h),
        effw=slider->w-2*sz,
        effh=slider->h-2*sz;
    float normv;
    if(is_tall){        
        normv=(y-slider->y-sz)/effh;
    } else {
        normv=(x-slider->x-sz)/effw;
    }
    slider->v=(1-normv)*slider->v_min+normv*slider->v_max;
    clamp(&slider->v,slider->v_min,slider->v_max);

    observable_mark(&slider->observable);
    update_geometry(slider);
}

static void slider_drag_init(struct slider *self) {
    self->draggable.hit=slider_drag_hit;
    self->draggable.set=slider_drag_set;
}

void slider_init(struct slider *slider) {
    if(!ctx_) ctx_init();

    slider->v_min=0.0f;
    slider->v_max=1.0f;
    slider->v=0.5f;
    slider_drag_init(slider);
    slider->viewport_listener.set=viewport_resize;
    observable_init(&slider->observable);

    // Prepping vertex buffers
    glGenVertexArrays(1,&slider->vao);
    glBindVertexArray(slider->vao);
    glGenBuffers(1,&slider->vbo);
    glBindBuffer(GL_ARRAY_BUFFER,slider->vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
    glBindVertexArray(0);

    mingl_check();
}

void slider_draw(struct slider *slider) {
    glUseProgram(ctx_->program);

    glUniform2f(ctx_->id.wh,(GLfloat)slider->w,(GLfloat)slider->h);

    glBindBuffer(GL_ARRAY_BUFFER,slider->vbo);
    glBindVertexArray(slider->vao);

    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    glDrawArrays(GL_TRIANGLE_FAN,4,4);

    glBindTexture(GL_TEXTURE_2D,0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);
}

static void update_geometry(const struct slider *slider) {
    const float
        x0=slider->x,
        y0=slider->y,
        x1=x0+slider->w,
        y1=y0+slider->h,
        u0=0.0f,v0=0.0f,u1=1.0f,v1=1.0f;    
    float sx0,sy0,sx1,sy1;
    get_handle_rect(slider,&sx0,&sy0,&sx1,&sy1);
    struct verts verts[8]={
        {x0,y0,u0,v0},
        {x0,y1,u0,v1},
        {x1,y1,u1,v1},
        {x1,y0,u1,v0},

        {sx0,sy0,0,0},
        {sx0,sy1,0,0},
        {sx1,sy1,0,0},
        {sx1,sy0,0,0},
    };
    glUseProgram(ctx_->program);    
    glBindBuffer(GL_ARRAY_BUFFER,slider->vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);
}

void slider_set_range(struct slider *slider,float v_min,float v,float v_max) {
    slider->v_min=v_min;
    slider->v=v;
    slider->v_max=v_max;
    observable_mark(&slider->observable);
    update_geometry(slider);
}

void slider_rect(struct slider *slider, float x,float y,float w,float h) {
    slider->x=x;
    slider->y=y;
    slider->w=w;
    slider->h=h;
    update_geometry(slider);
}

void slider_set(struct slider *slider,float v) {
    if(v==slider->v)
        return;
    slider->v=v;
    observable_mark(&slider->observable);
    update_geometry(slider);
}