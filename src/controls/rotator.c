#include "rotator.h"
#include "../pixelfont/src/mingl.h"
#include <math.h> //fabs,sqrtf
#include <stdio.h>

#define containerof(P,T,M)  ((T*)(((char*)P)-offsetof(T,M)))

struct verts{ float x,y,u,v; };

static void swap(struct accumulator* ctx);

//
// S H A D E R S
//

static const char FRAG[]=GLSL(
    in  vec2 uv;
    out vec4 color;
    uniform sampler2D im;

    void main(){
        color=texture(im,uv);
        color.a=color.r+color.g+color.b;
    }
);

static const char VERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    uniform vec2 size;
    out vec2 uv;

    void main(){
        gl_Position=vec4(2*(vert/size)-1,0,1.0f);
        gl_Position.y=-gl_Position.y;
        uv=tex;
    }
);

static const char MCFRAG[]=GLSL(
    layout(location=0) out vec4 color;
    uniform sampler2D accumulator;
    in  vec2 uv;

    uniform vec3 vx;
    uniform vec3 vy;
    uniform vec3 vz;
    uniform float minr;
    uniform float maxr;
    uniform float counts;
    uniform float size;

    vec2 hash23(vec3 p3) {
        p3 = fract(p3 * vec3(443.897, 441.423, 437.195));
        p3 += dot(p3, p3.yzx+19.19);
        return fract(vec2((p3.x + p3.y)*p3.z, (p3.x+p3.z)*p3.y))-0.5; // returns something between (-0.5,0.5)
    }

    vec2 lift(vec2 xy,vec3 xx, vec3 yy, vec3 zz) {
        // lift to plane orthogonal to rotation axis
        vec3 r=vec3(xy-vec2(0.5,0.5),0); // center unit box in the view
        r.z=-dot(zz.xy,r.xy)/zz.z;
        return vec2(dot(r,xx),dot(r,yy));
    }

    float accumulate(vec2 point, vec2 delta) {
            const float px=1.0/size;
            float vv=0.0;
            const vec2 p=point+delta*1;
            const float d=length(p);
            vv+=float((minr*px)<d) * float(d<=(maxr*px));

            float th=atan(p.y,p.x)+3.14159;
            vv+=float(0<th) * float(th<(0.05)) * float(d<(maxr*px));
            return vv;
    }

    const int NITER=1;
    void main(){
        vec4 v=texture(accumulator,uv);
        vec4 vv=vec4(0.0,0.0,0.0,1.0);
        vec2 p0,p1,p2;
    
        p0=lift(uv.xy,vx,vy,vz);
        p1=lift(uv.xy,vz,vx,vy);
        p2=lift(uv.xy,vy,vz,vx);
        for(int i=0;i<NITER;++i) {
            vec2 delta=hash23(vec3(uv.xy,(counts*10+i)*0.01))/size;
            vv.r+=accumulate(p0.xy,delta);        
            vv.g+=accumulate(p1.xy,delta);
            vv.b+=accumulate(p2.xy,delta);
        }        
        //color=vv/float(NITER);        
        color=mix(v,vv/float(NITER),1.0/counts);
    }
);

static const char MCVERT[]=GLSL(
    layout(location=0) in vec2 vert;
    layout(location=1) in vec2 tex;
    out vec2 uv;

    void main(){
        gl_Position=vec4(vert,0,1.0f);
        uv=tex;
    }
);

//
// D R A G   A N D   D R O P
//

static float dot3(const float *a, const float *b) {
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

static float dot2(const float *a, const float *b) {
    return a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
}

static void lift(const struct rotor* self,float x,float y,float out[2]) {
    // lift to plane orthogonal to rotation axis
    float r[3]={x-self->cx,y-self->cy,0};
    r[2]=-(self->z[0]*r[0]+self->z[1]*r[1])/self->z[2];
    out[0]=dot3(r,self->x);
    out[1]=dot3(r,self->y);
}

static int hit(const struct draggable *self_, int x, int y) {
    struct rotator* self=containerof(self_,struct rotator,draggable);
    self->drag_hit_rotor=-1;
    memcpy(self->drag_start_orientation,self->mat,9*sizeof(float));
    for(int i=0;i<3;++i) {
        // lift to plane orthogonal to rotation axis
        float r[2];
        lift(self->rotors+i,(float)x,(float)y,r);
        float dr=fabsf(sqrtf(r[0]*r[0]+r[1]*r[1])-self->rotors[i].r);
        if(dr<self->rotors[i].dr) {
            self->drag_hit_rotor=i;
            return 1;
        }
    }
    // no rotors hit.  check to see if ball hit.
    const float 
        dx=((float)x)-self->rotors[0].cx,// all the rotors have the same center
        dy=((float)y)-self->rotors[0].cy,
        r=sqrtf(dx*dx+dy*dy);
    if((self->rotors[0].r-r)>0)
        return 1;
    return 0;
}

static float norm(const float *a) {
    return sqrtf(a[1]*a[1]+a[0]*a[0]);
}

static void mul33(float *out, const float *a, const float *b) {
    for(int c=0;c<3;++c) {
        for(int r=0;r<3;++r) {
            float acc=0.0f;
            for(int i=0;i<3;++i)
                acc+=a[i+3*r]*b[c+3*i];
            out[c+3*r]=acc;
        }
    }
}
 
static void fmad33_ip(float *out, const float a, const float *b) {
    for(int c=0;c<3;++c)
        for(int r=0;r<3;++r)
            out[c+3*r]+=a*b[c+3*r];
}

static void cross(float out[3],const float a[3],const float b[3]) {
    out[0]=a[1]*b[2]-a[2]*b[1];
    out[1]=a[2]*b[0]-a[0]*b[2];
    out[2]=a[0]*b[1]-a[1]*b[0];
}

static float sign_cross2(const float *a,const float *b) {
    return -1.0f+2.0f*(float)((a[0]*b[1]-a[1]*b[0])>0);
}

static float det33(const float *a) {
    return a[0]*(a[3+1]*a[2*3+2]-a[1*3+2]*a[2*3+1])
          -a[1]*(a[3+0]*a[2*3+2]-a[1*3+2]*a[2*3+0])
          +a[2]*(a[3+0]*a[2*3+1]-a[1*3+1]*a[2*3+0]);
}

// Generates the rotation matrix, out, that rotates by theta about
// the vector k. Assumes k is normalized.
//
// out is a 3x3 matrix allocated/owned by the caller.
static void rotmat(float* out,const float k[3],const float theta) {
    // https://en.wikipedia.org/wiki/Rodrigues_rotation_formula
    const float K[9]={
                0, -k[2], k[1],
             k[2],     0,-k[0],
            -k[1],  k[0],    0};
    float K2[9];
    mul33(K2,K,K);
    const float e[]={1,0,0,0,1,0,0,0,1};
    memcpy(out,e,sizeof(e));
    fmad33_ip(out,sinf(theta),K);
    fmad33_ip(out,1.0f-cosf(theta),K2);
}

#define sign(a) ((a<0)?(-1):1)

static void normalize3(float *a) {
    const float n=sqrtf(a[0]*a[0]+a[1]*a[1]+a[2]*a[2])+1e-6f;
    a[0]/=n;
    a[1]/=n;
    a[2]/=n;
}

// a=m*a
static void m33v3_ip(const float *m33,float *a) {
    float out[3];
    for(int r=0;r<3;++r) {
        float acc=0.0;
        for(int i=0;i<3;++i)
            acc+=m33[3*r+i]*a[i];
        out[r]=acc;
    }
    memcpy(a,out,sizeof(out));
}

static float clamp(const float v, const float min, const float max) {
    return (v<min)?min:((v>max)?max:v);
}

// Compute in plane rotation when dragging from point a to point b with the rotor.
// Assumes a and b have been translated to the center of the rotor is the origin (zero).
static float dtheta_in_plane(float ax,float ay,float bx,float by) {
    const float 
        a2=ax*ax+ay*ay,
        b2=bx*bx+by*by,
        dx=ax-bx,
        dy=ay-by,
        c2=dx*dx+dy*dy,
        denom=2.0f*sqrtf(a2*b2+1e-6f),
        th=acosf(clamp((a2+b2-c2)/denom,-1,1)),
        cross=bx*ay-ax*by;
    return sign(cross)*th;
}

// Compute out of plane rotation when dragging from point a to point b with the rotor.
// Assumes a and b have been translated to the center of the rotor is the origin (zero).
static float dtheta_out_of_plane(const float *z,float ax,float ay,float bx,float by) {
    const float 
        a=sqrtf(ax*ax+ay*ay),
        az=-(z[0]*ax+z[1]*ay)/(z[2]+1e-6f), // lift a to plane
        R2=ax*ax+ay*ay+az*az;     // radius sq. of grabbed sphere
    // check if b is outside, and remap if necessary 
    const float
        lb2=bx*bx+by*by;     // squared length of b 
    // Project the problem into the a plane.
    float
        b=(bx*ax+by*ay)/a,        // dot(a,b)/|a| -> projection of b onto a   
        bz=sqrtf(R2-b*b);         // lift b to sphere
    if(b*b>R2) {
        const float ratio=sqrtf(R2/b/b);
        b*=ratio;
        bz=0.0f;
    }
    const float
        dab=a-b,
        dabz=az-bz,
        lab2=dab*dab+dabz*dabz,   // length of the a to b vector (in a plane)
        // sign of b cross a determines sign of theta.
        // in plane a:(a,az,0) and b:(b,bz,0).
        c=b*az-a*bz, // cross-product z component (the only component)
        theta=acosf(1.0f-0.5f*lab2/R2);
    return sign(c)*theta;
}

static void set(const struct draggable *self_,float x0,float y0,float x1,float y1) {
    struct rotator* self=containerof(self_,struct rotator,draggable);
    const float 
            ax=x0-self->rotors[0].cx,
            ay=y0-self->rotors[0].cy,
            bx=x1-self->rotors[0].cx,
            by=y1-self->rotors[0].cy;
    float T[9];
    if(self->drag_hit_rotor>=0) {
        const struct rotor *rotor=self->rotors+self->drag_hit_rotor;
        
        // Use law of cosines to get interior angle between r0 and r1
        // a is center to r0. b is center to r1. c is a to b. center is (0,0).
        // This doesn't give us a signed angle, so use cross product for sign.
        float dth=dtheta_in_plane(ax,ay,bx,by);
        
        rotmat(T,rotor->z,dth);
        mul33(self->mat,self->drag_start_orientation,T);
        self->last_dth=dth;
    } else {
        // Not twisting a rotor.
        // 1. lift a and b to sphere, normalize
        // 2. use cross product to get angle (a x b=sin(th)*n)
        // 3. when b is outside sphere,bz=0
        const float
            R=self->rotors[0].r,
            R2=R*R,
            R3=R2*R,
            az=sqrtf(R2-ax*ax-ay*ay),
            b2=bx*bx+by*by,
            bz=((R2-b2)>0.1f)?sqrtf(R2-bx*bx-by*by):0.0f,
            nx=ay*bz-by*az,
            ny=bx*az-ax*bz,
            nz=ax*bz-bx*az,
            n =sqrtf(nx*nx+ny*ny+nz*nz),
            v[3]={nx/n,ny/n,nz/n},
            dth=asinf(clamp(n/R2,-1,1));
        if(isnan(dth))
            DebugBreak();
        rotmat(T,v,dth);
        mul33(self->mat,self->drag_start_orientation,T);
        self->last_dth_ortho=dth;
    }

    struct rotator_view *view=&self->view;
    swap(&view->accumulator);
    glViewport(0,0,view->viewport_listener.w,view->viewport_listener.h); // restore viewport. dirtied by accumulator
    observable_mark(&self->observable);
}

//
// I N I T I A L I Z A T I O N
//

static void init_display(struct display *ctx) {
    ctx->program=mingl_new_program(FRAG,sizeof(FRAG),VERT,sizeof(VERT),0,0);
    ctx->id.size =glGetUniformLocation(ctx->program,"size");
    glUseProgram(ctx->program);
    glUniform1i(glGetUniformLocation(ctx->program,"im"),0);
    {
        // This is too slow for a draw, but we can afford to directly
        // query the viewport during initialization.
        GLint sz[4]={0};
        glGetIntegerv(GL_VIEWPORT,sz);
        glUniform2f(ctx->id.size,(GLfloat)sz[2],(GLfloat)sz[3]);
    }
    glUseProgram(0);

    // Prep vertex buffers
    glGenBuffers(1,&ctx->vbo);
    glGenVertexArrays(1,&ctx->vao);
    glBindVertexArray(ctx->vao);
    glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
    glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
    glBindVertexArray(0);
}

static void init_accumulator(struct accumulator *ctx,float r, float dr) {
    ctx->counts=0;
    ctx->w=ctx->h=50;
    ctx->program=mingl_new_program(MCFRAG,sizeof(MCFRAG),MCVERT,sizeof(MCVERT),0,0);
    ctx->id=(struct accumulator_uniforms) {
        .counts=glGetUniformLocation(ctx->program,"counts"),
        .vx=glGetUniformLocation(ctx->program,"vx"),
        .vy=glGetUniformLocation(ctx->program,"vy"),
        .vz=glGetUniformLocation(ctx->program,"vz"),
        .size=glGetUniformLocation(ctx->program,"size"),
        .minr=glGetUniformLocation(ctx->program,"minr"),
        .maxr=glGetUniformLocation(ctx->program,"maxr"),
    };
    glUseProgram(ctx->program);
    glUniform1i(glGetUniformLocation(ctx->program,"accumulator"),0);
    glUniform3f(ctx->id.vx,1,0,0);
    glUniform3f(ctx->id.vy,0,1,0);
    glUniform3f(ctx->id.vz,0,0,1);
    glUniform1f(ctx->id.size,(float)ctx->w);
    glUniform1f(ctx->id.minr,r-dr);
    glUniform1f(ctx->id.maxr,r+dr);
    glUseProgram(0);    

    glGenFramebuffers(1,&ctx->fbo);
    glGenTextures(2,ctx->tex);

    for(int i=0;i<2;++i) {
        glBindTexture(GL_TEXTURE_2D,ctx->tex[i]);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,ctx->w,ctx->h,0,GL_RGB,GL_FLOAT,0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }    

    glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ctx->tex[0], 0);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        OutputDebugStringA("ruh roh!\n"); // FIXME: need better error handling
        return;
    }

    { 
        // prep vertex buffer for rendering to texture 
        glGenBuffers(1,&ctx->vbo);
        glGenVertexArrays(1,&ctx->vao);
        glBindVertexArray(ctx->vao);
        glActiveTexture(GL_TEXTURE0);
        glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),0);
        glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,sizeof(struct verts),(void*)offsetof(struct verts,u));
        glBindVertexArray(0);

        const float 
            x0=-1.0f,y0=-1.0f,x1=1.0f,y1=1.0f,
            u0= 0.0f,v0= 0.0f,u1=1.0f,v1=1.0f;
        struct verts verts[4]={
            {x0,y0,u0,v0},
            {x0,y1,u0,v1},
            {x1,y1,u1,v1},
            {x1,y0,u1,v0},
        };
        glUseProgram(ctx->program);
        glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
        glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER,0);
        glUseProgram(0);
    }
}


static void set_viewport(struct viewport_listener *self,float w, float h) {
    struct rotator_view *view=containerof(self,struct rotator_view,viewport_listener);
    view->viewport_listener.w=w;
    view->viewport_listener.h=h;
    glUseProgram(view->display.program);
    glUniform2f(view->display.id.size,(GLfloat)w,(GLfloat)h);
    glUseProgram(0);
}

static void init_view(struct rotator_view *view,float r,float dr) {
    view->viewport_listener.set=set_viewport;
    init_display(&view->display);
    init_accumulator(&view->accumulator,r,dr);
    if(!mingl_check())
        goto OpenGLError;
OpenGLError:
    // TODO: add error handling
    ;
}

void rotator_init(struct rotator *rotator,float r,float dr) {
    rotator->draggable=(struct draggable){
            .hit=hit,
            .set=set
    };
    const struct rotor rotor_prototype={
        .r=r,
        .dr=dr
    };
    for(int i=0;i<3;++i) {
        struct rotor *r=rotator->rotors+i;
        *r=rotor_prototype;
        r->idx=i;
        r->x=rotator->drag_start_orientation+3*i;
        r->y=rotator->drag_start_orientation+3*((i+1)%3);
        r->z=rotator->drag_start_orientation+3*((i+2)%3);
    }
    memset(rotator->mat,0,sizeof(float)*9);
    rotator->mat[0]=1;
    rotator->mat[4]=1;
    rotator->mat[8]=1;
    
    observable_init(&rotator->observable);
    init_view(&rotator->view,r,dr);
}

// 
// D R A W I N G
//

static void accumulate(struct accumulator* ctx) {
    ++ctx->counts;
    // render to fbo
    glViewport(0,0,ctx->w,ctx->h);
    glUseProgram(ctx->program);
    glUniform1f(ctx->id.counts,(float)ctx->counts);
    glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,ctx->tex[0]);
    glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
    glBindVertexArray(ctx->vao);
    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
}

static void display(const struct display* ctx,unsigned tex) {    
    glUseProgram(ctx->program);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,tex);
    glBindBuffer(GL_ARRAY_BUFFER,ctx->vbo);
    glBindVertexArray(ctx->vao);
    glDrawArrays(GL_TRIANGLE_FAN,0,4);
    glBindTexture(GL_TEXTURE_2D,0);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glUseProgram(0);
}

void rotator_draw(struct rotator *rotator) {
    struct rotator_view *view=&rotator->view;

    glUseProgram(view->accumulator.program);
    glUniform3fv(view->accumulator.id.vx,1,rotator->mat);
    glUniform3fv(view->accumulator.id.vy,1,rotator->mat+3);
    glUniform3fv(view->accumulator.id.vz,1,rotator->mat+6);
    glUseProgram(0);

    accumulate(&view->accumulator);
    glViewport(0,0,view->viewport_listener.w,view->viewport_listener.h); // restore viewport. dirtied by accumulator
    display(&view->display,view->accumulator.tex[0]); // swap?
}


static void swap(struct accumulator* ctx) {
    ctx->counts=0;
    // clear fbo
    glBindFramebuffer(GL_FRAMEBUFFER,ctx->fbo);    
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, ctx->tex[0], 0);
    glViewport(0,0,ctx->w,ctx->h);
    glClearColor(0,0,1,0);
    glClear(GL_COLOR_BUFFER_BIT);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
}

void rotator_notify(struct rotator *rotator) {
    struct rotator_view *view=&rotator->view;
    swap(&view->accumulator);
    glViewport(0,0,view->viewport_listener.w,view->viewport_listener.h); // restore viewport
}

static void rotator_view_set_rect(struct rotator_view* view, float x0, float y0, float w, float h) {
    const float
        x1=x0+w,
        y1=y0+h,
        u0=0.0f,v0=0.0f,u1=1.0f,v1=1.0f;
    struct verts verts[4]={
        {x0,y0,u0,v0},
        {x0,y1,u0,v1},
        {x1,y1,u1,v1},
        {x1,y0,u1,v0},
    };    
    glUseProgram(view->display.program);
    glBindBuffer(GL_ARRAY_BUFFER,view->display.vbo);
    glBufferData(GL_ARRAY_BUFFER,sizeof(verts),verts,GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER,0);

    struct accumulator *accumulator=&view->accumulator;
    accumulator->w=(unsigned)w;
    accumulator->h=(unsigned)h;
    for(int i=0;i<2;++i) {
        glBindTexture(GL_TEXTURE_2D,accumulator->tex[i]);
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,accumulator->w,accumulator->h,0,GL_RGB,GL_FLOAT,0);
    }
    glUseProgram(accumulator->program);
    glUniform1f(accumulator->id.size,(float)accumulator->w);
    glUseProgram(0);

    swap(accumulator);
    glViewport(0,0,view->viewport_listener.w,view->viewport_listener.h); // restore viewport
}

void rotator_rect(struct rotator *rotator, float x0, float y0, float w, float h) {
    rotator_view_set_rect(&rotator->view,x0,y0,w,h);
    for(int i=0;i<3;++i) {
        rotator->rotors[i].cx=x0+w/2.0f;
        rotator->rotors[i].cy=y0+h/2.0f;
    }
}


/*
NOTES

     - NOT double buffering right now even though I setup both textures
     - Something is weird/wrong with how I think about rendering to the fbo
        - For example, if I clear the accumulator texture and then draw to it,
          I see (mostly?) the clear color.  Maybe there's a hint of the 
          accumulated stuff.  But the clear color should be completely
          overwritten (with accumulation off)...

FIXME

    - drag and drop needs to consider the dtheta and dr.  
*/