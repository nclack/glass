Work in progress
================

Large volume support
--------------------

I want to try to load some larger volumes and also excercise KLB's sub-volume support.

 - chunk up cache requests so we don't have to wait on the full volume
    - FileSeries implementation doesn't use Reader's chunk size...
      - This will probably mean that one time point has to have a representative chunk size
    - I think it currently tries to cache entire timepoints.
      The idea would be to (a) use an accurate unit size and (b) properly serve requests.
    - It seems like the interface will require requesting a certain shape.
      Right now it's just based on an offset...
        - I think the idea was that whatever chunk corresponds to the aligned offset gets 
          returned and the caller has to respond appropriately.
        - A: chunk compositing
          B: generating offsets within a bounding box
 - what to do about changing depth
 - "Buffer is too small to cache 1 item"
 - GL_OUT_OF_MEMORY
    - when do I hit this?
    - how to detect and respond



TODO
====
   - add a config file
   - add resource file for icons etc

   - adjust shader accumulation parameters according to performance
   - time texture upload

   - improved contrast slider

   - cropping of the input bounding box
   - pan
   - zoom

   - voxel scale
   - rotation behavior to vshow   
     - refactor different rotation beahviors
   - layout
   - change accumulator resolution
        ???  Can do this but I don't understand something about what happens to the uv scaling on the accumulator
   
   - multiview
   - multichannel
   - color mapping

   - bounding box preview

   - play controls for time series

   - ffmpeg?

ISSUES
======

    - Cache: when cycling through requests faster than they can be satisfied,
      the request queue sort of just fills up indefinitely...Need to do something
      to avoid redundant requests.

    - Need to test that KLB and Tiff Reader interfaces satisfy requirements

    - currently the z plane control is in pixels.  Properly this should just be 0 to 1.
      since this operates in the view space
    - drag is lost when I leave the window

      
IDEAS
=====

    - transfer function?
    - volumes exceeding gpu memory
        - how to iteratively add volumes to the same accumulator
        - what if I were incrementally updating from a key-value 
          subvolume store...adding volumes as they arrive...

    - hierarchical scale cache
    - pre-cache (low priority request queue that fills in gaps and then expands to fill)

    - annotate area in volume projection as background to set
      contrast min/transfer function

    - Usability
        - when would I use this in practice
          - as a debugging/visualization tool?

    - different ways of statistically aggregating xy vs z

    - aligning/showing multiple timeseries
      - for example overlaying the correlation result with the source video

    - how to separate this out into different products (product ideas)
       - vshow for matlab/python (like imshow)
       - exploration/compositing/annotation (like bigdataviewer?)

LOOK IN TO
==========

PBOs: http://www.songho.ca/opengl/gl_pbo.html
  for texture streaming?

Testing
=======

Mock a reader and write tests for CachedReader and FileSeries